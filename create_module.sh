#!/bin/bash

# The folder path where modules should be created
root="./lib/modules/"
# The first command parameter -> sh create_module.sh my_module_name
moduleName="$1"
# The second command parameter -> sh create_module.sh my_module_name my_sub_module_name
subModuleName="$2"
# The created module folder path
moduleDirectory="$root$moduleName"
# Init the module file name
moduleFileName="$moduleName"

echo "--------------------------------------------------------------------------------"

if [[ -z "$moduleName" ]];
    then
      # Stop script if you don't provided any parameter :(
      echo "ERROR: You must provide a module name, please try: sh create_module.sh my_module_name" 1>&2
      echo "--------------------------------------------------------------------------------"
      exit 1
    else
      # Stop script if you provided a wrong parameter :/
      if [[ "${moduleName}" =~ [^a-z_] ]];
          then
            echo "ERROR: You must provide a correct module name with only lower case letters and underscores" 1>&2
            echo "--------------------------------------------------------------------------------"
          exit 1
      fi
fi

if [[ -z "$subModuleName" ]];
    then
      # Display a hint if you don't provided the second parameter :)
      echo "HINT: Do you know that you can create a sub module with: sh create_module.sh my_module_name my_sub_module_name"
      echo "--------------------------------------------------------------------------------"
    else
      # Stop script if you provided a wrong second parameter :/
      if [[ "${subModuleName}" =~ [^a-z_] ]];
          then
            echo "ERROR: You must provide a correct sub module name with only lower case letters and underscores" 1>&2
            echo "--------------------------------------------------------------------------------"
            exit 1
      fi
      # Update the created module folder path with sub module
      moduleDirectory="$moduleDirectory/$subModuleName"
      # Update the module file name with sub module
      moduleFileName="$moduleName""_$subModuleName"
fi

if [ -d "$moduleDirectory" -a ! -h "$moduleDirectory" ]
then
   # Stop script if the module folder directory already exists :s
   echo "ERROR: $moduleDirectory already exists" 1>&2
   echo "--------------------------------------------------------------------------------"
   exit 1
fi

# Split module file name every "_"
splitNames=(${moduleFileName//_/ })
# Init module class name
moduleClassName=""
# For each word of our split module name
for name in "${splitNames[@]}"
  do
    # Uppercase first letter
    upperCasedName="$(tr '[:lower:]' '[:upper:]' <<< ${name:0:1})${name:1}"
    # Add the word to our module class name
    moduleClassName="$moduleClassName$upperCasedName"
  done

# Hell yeah ! We can start creating our module \m/
echo "STEP 1: Create the folder $moduleDirectory"
echo "--------------------------------------------------------------------------------"
mkdir -p "$moduleDirectory"

# Generate our logic file
echo "STEP 2: Generate $moduleDirectory/$moduleFileName""_logic.dart"
echo "--------------------------------------------------------------------------------"
cat > "$moduleDirectory/$moduleFileName""_logic.dart" << EOL
import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '${moduleFileName}_view.dart';

part '${moduleFileName}_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class ${moduleClassName}Event with _\$${moduleClassName}Event {
  static StreamController<${moduleClassName}Event>? streamController;

  static final provider = StreamProvider<${moduleClassName}Event>((ref) {
    streamController = StreamController<${moduleClassName}Event>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });

  const factory ${moduleClassName}Event.max([String? message]) = ${moduleClassName}EventMax;
}

class ${moduleClassName}Logic extends StateNotifier<${moduleClassName}State> {
  static final provider = StateNotifierProvider.family<${moduleClassName}Logic, ${moduleClassName}State, int>(
      (ref, maxCounterValue) =>
          ${moduleClassName}Logic(initialState: const ${moduleClassName}State(counter: 0), maxCounterValue: maxCounterValue));

  ${moduleClassName}Logic({required ${moduleClassName}State initialState, required this.maxCounterValue}) : super(initialState);

  final int maxCounterValue;

  void incrementCounter() {
    if (state.counter >= maxCounterValue) {
      ${moduleClassName}Event.streamController!.add(const ${moduleClassName}Event.max('OMFG you reached max counter value'));
    } else {
      state = state.copyWith(counter: state.counter + 1);
    }
  }
}
EOL

# Generate our view file
echo "STEP 3: Generate $moduleDirectory/$moduleFileName""_view.dart"
echo "--------------------------------------------------------------------------------"
cat > "$moduleDirectory/$moduleFileName""_view.dart" << EOL
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '${moduleFileName}_logic.dart';

part '${moduleFileName}_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class ${moduleClassName}State with _\$${moduleClassName}State {
  const factory ${moduleClassName}State({
    required int counter,
  }) = _${moduleClassName}State;

  const ${moduleClassName}State._();
}

class ${moduleClassName}View extends ConsumerWidget {
  const ${moduleClassName}View({super.key, required this.maxCounterValue});

  final int maxCounterValue;

  void onEvent(${moduleClassName}Event e) {
    if (e is ${moduleClassName}EventMax && e.message != null) {
      debugPrint('[${moduleClassName}] ERROR: \${e.message}');
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    final logic = ref.read(${moduleClassName}Logic.provider(maxCounterValue).notifier);
    ref.listen<AsyncValue<${moduleClassName}Event>>(${moduleClassName}Event.provider, (previous, next) => onEvent(next.value!));
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: logic.incrementCounter,
        child: const Text('+'),
      ),
      body: Center(
        child: Consumer(
          builder: (context, ref, child) {
            final state = ref.watch(${moduleClassName}Logic.provider(maxCounterValue));
            return Text('Counter: \${state.counter}');
          },
        ),
      ),
    );
  }
}
EOL

# Well done you generated a module :D
echo "DONE!"
echo "--------------------------------------------------------------------------------"
echo "Now you should run this command to generate view's state and logic's event classes:"
echo "flutter pub run build_runner build --delete-conflicting-outputs"
echo "--------------------------------------------------------------------------------"
echo "Then add a new route to your router and reach this route to test your new module"
echo "--------------------------------------------------------------------------------"