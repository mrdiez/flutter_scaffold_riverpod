import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/modules/home/home_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/texts/section_title.dart';

class HomeFamilySection extends ConsumerWidget {
  const HomeFamilySection({super.key, required this.maxCounterValue});

  final int maxCounterValue;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SectionTitle(text: context.translation.familyProviderExample),
        $Gap.s,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {
                final logic = ref.read(HomeLogic.familyProvider(maxCounterValue).notifier);
                logic.incrementCounter();
              },
              child: Text(context.translation.increment),
            ),
            Consumer(builder: (context, ref, child) {
              final state = ref.watch(HomeLogic.familyProvider(maxCounterValue));
              return Text(state.counter.toString(), style: context.textTheme.headline1);
            }),
          ],
        ),
        $Gap.xxxl,
      ],
    );
  }
}
