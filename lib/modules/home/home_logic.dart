import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'home_view.dart';

part 'home_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class HomeEvent with _$HomeEvent {
  static StreamController<HomeEvent>? streamController;

  static final provider = StreamProvider<HomeEvent>((ref) {
    streamController = StreamController<HomeEvent>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });

  const factory HomeEvent.max([String? message]) = HomeEventMax;
}

///
/// Note: in this example to be even more simple we could remove the class [HomeState]
/// and replace it with just a [int] like this: class HomeLogic extends StateNotifier<int>
/// and directly return the counter value as state instead of having a dedicated class
///
class HomeLogic extends StateNotifier<HomeState> {
  ///
  /// A simple provider will cache your state
  /// You can call this getter anywhere at anytime you will always get the same [HomeState] and [HomeLogic] instance
  /// except if you're watching another provider in its builder/factory (see [nestedProvider])
  ///
  static final simpleProvider =
      StateNotifierProvider<HomeLogic, HomeState>((ref) => HomeLogic(initialState: const HomeState(counter: 0)));

  ///
  /// A family provider allow you to pass a parameter
  /// By calling this getter with a parameter with the same hashcode you will get the same state
  /// Here the parameter will define the max value that our counter can reach
  ///
  static final familyProvider = StateNotifierProvider.family<HomeLogic, HomeState, int>(
      (ref, maxCounterValue) => HomeLogic(initialState: const HomeState(counter: 0), maxCounterValue: maxCounterValue));

  ///
  /// An auto dispose provider will loose his state
  /// And so calling again this getter will provide you brand new [HomeState] and [HomeLogic] instances
  /// Open the complete example and come back to this page then check that this counter has been reset
  ///
  static final autoDisposeProvider = StateNotifierProvider.autoDispose<HomeLogic, HomeState>(
      (ref) => HomeLogic(initialState: const HomeState(counter: 0)));

  ///
  /// Here is another family provider (now the parameter is supposed to define our counter's initial value)
  /// This provider is watching another provider's state in his builder/factory so that it will be reset
  /// with new [HomeState] and [HomeLogic] instances each time the watched provider will provide a new value/state
  ///
  static final nestedProvider = StateNotifierProvider.family<HomeLogic, HomeState, int>((ref, initialValue) {
    /// Note the [.select] method which allow you to only watch a specific property of the state if needed
    final incrementValue = ref.watch(simpleProvider.select((state) => state.counter));
    return HomeLogic(
      initialState: HomeState(counter: initialValue),
      incrementValue: incrementValue,
    );
  });

  HomeLogic({
    required HomeState initialState,
    this.maxCounterValue,
    this.incrementValue = 1,
  }) : super(initialState);

  final int? maxCounterValue;
  final int incrementValue;

  void incrementCounter() {
    if (maxCounterValue != null && state.counter >= maxCounterValue!) {
      HomeEvent.streamController!.add(const HomeEvent.max('OMFG'));
    } else {
      state = state.copyWith(counter: state.counter + incrementValue);
    }
  }
}
