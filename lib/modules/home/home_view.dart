import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/config/routes.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/modules/home/widgets/home_auto_dispose_section.dart';
import 'package:scaffold_riverpod/modules/home/widgets/home_family_section.dart';
import 'package:scaffold_riverpod/modules/home/widgets/home_nested_section.dart';
import 'package:scaffold_riverpod/modules/home/widgets/home_simple_section.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/images/logo.dart';
import 'package:scaffold_riverpod/widgets/texts/section_title.dart';

import 'home_logic.dart';

part 'home_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class HomeState with _$HomeState {
  const factory HomeState({
    required int counter,
  }) = _HomeState;

  const HomeState._();
}

class HomeView extends ConsumerWidget {
  const HomeView({super.key});

  void onEvent(HomeEvent e, BuildContext context) => e.when(
        max: (message) => context.showWarningSnackBar(
            '${context.translation.youReachedMaxValue}${e.message.isNotNullOrBlank ? '\n${e.message}' : ''}'),
      );

  @override
  Widget build(BuildContext context, ref) {
    ref.listen<AsyncValue<HomeEvent>>(HomeEvent.provider, (previous, next) => onEvent(next.value!, context));
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: $Padding.m),
        child: ListView(
          children: [
            $Gap.l,
            Logo(axis: Axis.vertical, height: context.screenSize.width / 4),
            $Gap.xxxl,
            const HomeSimpleSection(),
            const HomeFamilySection(maxCounterValue: 10),
            const HomeAutoDisposeSection(),
            const HomeNestedSection(initialValue: 666),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SectionTitle(text: context.translation.completeExample),
                $Gap.s,
                Center(
                  child: ElevatedButton(
                    child: const Text('Go to /customers'),
                    onPressed: () => context.go(customerListRoute),
                  ),
                ),
              ],
            ),
            $Gap.xl,
          ],
        ),
      ),
    );
  }
}
