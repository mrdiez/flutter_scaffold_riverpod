import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/modules/customer/list/customer_list_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CustomerSearchBar extends ConsumerWidget {
  const CustomerSearchBar({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerListLogic.provider.notifier);
    return SizedBox(
      height: $InputHeight.s,
      child: Material(
        borderRadius: const BorderRadius.all($Radius.m),
        color: context.theme.inputDecorationTheme.fillColor,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: TextField(
                controller: logic.textEditingController,
                style: context.textTheme.bodyText1,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                    left: $Padding.s,
                    right: $Padding.s,
                    bottom: $Padding.s,
                    top: $Padding.xxs,
                  ),
                  constraints: const BoxConstraints.tightFor(height: $InputHeight.s),
                  hintText: context.translation.searchCustomer,
                  hintStyle: context.textTheme.bodyText2,
                  border: InputBorder.none,
                ),
                onChanged: logic.search,
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Consumer(builder: (context, ref, child) {
                final search = ref.watch(CustomerListLogic.provider.select((s) => s.search));
                return search.isNotNullOrBlank
                    ? IconButton(
                        icon: const Icon(
                          Icons.close_rounded,
                          size: $Icon.s,
                        ),
                        onPressed: logic.reset,
                      )
                    : Container();
              }),
            ),
          ],
        ),
      ),
    );
  }
}
