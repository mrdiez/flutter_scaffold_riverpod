import 'package:flutter/material.dart';
import 'package:scaffold_riverpod/modules/customer/list/widgets/customer_search_bar.dart';

class CustomerListAppBar extends AppBar {
  CustomerListAppBar({super.key});

  @override
  State<CustomerListAppBar> createState() => _CustomerListAppBarState();
}

class _CustomerListAppBarState extends State<CustomerListAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const CustomerSearchBar(),
    );
  }
}
