import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/config/routes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/layout/rounded_material.dart';

class CustomerListItem extends StatelessWidget {
  const CustomerListItem(this.customer, {Key? key}) : super(key: key);

  final Customer customer;

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      heroTag: 'customer_container_${customer.id}',
      onlyBackgroundHero: true,
      innerPadding: EdgeInsets.zero,
      onTap: () => context.go(
        customerDetailsRoute(customer.id),
        extra: {'customer': customer},
      ),
      child: ListTile(
        leading: Hero(
          tag: 'customer_avatar_${customer.id}',
          child: CircleAvatar(child: Text(customer.initials)),
        ),
        title: Row(
          children: [
            Hero(
              tag: 'customer_first_name_${customer.id}',
              child: AutoSizeText(
                '${customer.firstName} ',
                style: context.textTheme.subtitle1,
                maxLines: 1,
              ),
            ),
            Expanded(
              child: Hero(
                tag: 'customer_last_name_${customer.id}',
                child: AutoSizeText(
                  customer.lastName,
                  style: context.textTheme.subtitle1,
                  maxLines: 1,
                ),
              ),
            ),
          ],
        ),
        subtitle: Hero(
          tag: 'customer_subtitle_${customer.id}',
          child: Text(customer.subtitle),
        ),
      ),
    );
  }
}
