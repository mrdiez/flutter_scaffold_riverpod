import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:scaffold_riverpod/data/access/customer_repository.dart';
import 'package:scaffold_riverpod/utils/string.dart';

import 'customer_list_view.dart';

part 'customer_list_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerListEvent with _$CustomerListEvent {
  static StreamController<CustomerListEvent>? streamController;

  static final provider = StreamProvider<CustomerListEvent>((ref) {
    streamController = StreamController<CustomerListEvent>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });

  const factory CustomerListEvent.unknownError([String? message]) = CustomerListEventUnknownError;
}

class CustomerListLogic extends StateNotifier<CustomerListState> {
  static final provider = StateNotifierProvider<CustomerListLogic, CustomerListState>(
    (ref) => CustomerListLogic(
      initialState: const CustomerListState(loading: true),
      customerRepository: ref.watch(CustomerRepository.provider),
    ),
  );

  CustomerListLogic({required CustomerListState initialState, required this.customerRepository}) : super(initialState) {
    customerRepository.watchAll().listen(
      (customers) {
        state = state.copyWith(customers: customers, filteredCustomers: customers, loading: false);
      },
      onError: (error, stackTrace) => state = state.copyWith(error: true),
    );
  }

  final CustomerRepository customerRepository;
  final TextEditingController textEditingController = TextEditingController();

  void reset() {
    search(null);
    textEditingController.value = const TextEditingValue(text: '');
  }

  void search(String? search) {
    if (search.isNullOrBlank) {
      state = state.copyWith(search: null, filteredCustomers: state.customers);
      return;
    }
    final searchedWords = search!.cleanAndSplit();
    final result = state.customers.where((c) {
      List<String> foundWords = _searchWordsInString(c.name, searchedWords);
      if (foundWords.length == searchedWords.length) return true;
      return false;
    });
    state = state.copyWith(search: search, filteredCustomers: result.toList());
  }

  List<String> _searchWordsInString(String str, List<String> searchedWords) {
    List<String> foundWords = [];
    final words = str.cleanAndSplit();
    if (words.join().contains(searchedWords.join())) return searchedWords;
    for (final w in words) {
      for (final sw in searchedWords) {
        final result = w.contains(sw);
        if (result && !foundWords.contains(sw)) foundWords.add(sw);
      }
    }
    return foundWords;
  }

  @override
  void dispose() {
    super.dispose();
  }
}
