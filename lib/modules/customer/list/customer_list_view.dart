import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/config/routes.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/list/widgets/customer_list_app_bar.dart';
import 'package:scaffold_riverpod/modules/customer/list/widgets/customer_list_item.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/utils/providers.dart';

import 'customer_list_logic.dart';

part 'customer_list_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerListState with _$CustomerListState {
  const factory CustomerListState({
    @Default([]) List<Customer> customers,
    @Default([]) List<Customer> filteredCustomers,
    String? search,
    @Default(false) bool loading,
    @Default(false) bool error,
  }) = _CustomerListState;

  String get filteredCustomerIds => filteredCustomers.map((c) => c.id).join();

  const CustomerListState._();
}

class CustomerListView extends ConsumerWidget {
  const CustomerListView({super.key});

  void onEvent(CustomerListEvent e) {
    if (e is CustomerListEventUnknownError && e.message.isNotNullOrBlank) {
      debugPrint(e.message);
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AsyncValue<CustomerListEvent>>(CustomerListEvent.provider, (previous, next) => onEvent(next.value!));
    return Scaffold(
      appBar: CustomerListAppBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.go(customerCreateRoute),
        child: const Icon(Icons.add_rounded, size: $Icon.m),
      ),
      body: Consumer(
        builder: (context, ref, child) {
          ref.watch(
              CustomerListLogic.provider.multiSelect((s) => [s.error, s.loading, s.filteredCustomerIds, s.customers]));
          final state = ref.read(CustomerListLogic.provider);
          return state.error
              ? Center(child: Text(context.translation.unknownError))
              : state.loading
                  ? const Center(child: CircularProgressIndicator())
                  : ListView.builder(
                      itemCount: state.filteredCustomers.length,
                      itemBuilder: (context, i) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: $Padding.m, horizontal: $Padding.m),
                          child: CustomerListItem(state.filteredCustomers[i]),
                        );
                      },
                    );
        },
      ),
    );
  }
}
