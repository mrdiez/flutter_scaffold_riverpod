// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'customer_edit_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomerEditState {
  Customer? get customer => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerEditStateCopyWith<CustomerEditState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerEditStateCopyWith<$Res> {
  factory $CustomerEditStateCopyWith(
          CustomerEditState value, $Res Function(CustomerEditState) then) =
      _$CustomerEditStateCopyWithImpl<$Res, CustomerEditState>;
  @useResult
  $Res call({Customer? customer, bool error});
}

/// @nodoc
class _$CustomerEditStateCopyWithImpl<$Res, $Val extends CustomerEditState>
    implements $CustomerEditStateCopyWith<$Res> {
  _$CustomerEditStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customer = freezed,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      customer: freezed == customer
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as Customer?,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CustomerEditStateCopyWith<$Res>
    implements $CustomerEditStateCopyWith<$Res> {
  factory _$$_CustomerEditStateCopyWith(_$_CustomerEditState value,
          $Res Function(_$_CustomerEditState) then) =
      __$$_CustomerEditStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Customer? customer, bool error});
}

/// @nodoc
class __$$_CustomerEditStateCopyWithImpl<$Res>
    extends _$CustomerEditStateCopyWithImpl<$Res, _$_CustomerEditState>
    implements _$$_CustomerEditStateCopyWith<$Res> {
  __$$_CustomerEditStateCopyWithImpl(
      _$_CustomerEditState _value, $Res Function(_$_CustomerEditState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customer = freezed,
    Object? error = null,
  }) {
    return _then(_$_CustomerEditState(
      customer: freezed == customer
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as Customer?,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_CustomerEditState extends _CustomerEditState {
  const _$_CustomerEditState({this.customer, this.error = false}) : super._();

  @override
  final Customer? customer;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'CustomerEditState(customer: $customer, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CustomerEditState &&
            (identical(other.customer, customer) ||
                other.customer == customer) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, customer, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CustomerEditStateCopyWith<_$_CustomerEditState> get copyWith =>
      __$$_CustomerEditStateCopyWithImpl<_$_CustomerEditState>(
          this, _$identity);
}

abstract class _CustomerEditState extends CustomerEditState {
  const factory _CustomerEditState(
      {final Customer? customer, final bool error}) = _$_CustomerEditState;
  const _CustomerEditState._() : super._();

  @override
  Customer? get customer;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_CustomerEditStateCopyWith<_$_CustomerEditState> get copyWith =>
      throw _privateConstructorUsedError;
}
