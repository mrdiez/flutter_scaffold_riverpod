import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/edit/customer_edit_logic.dart';
import 'package:scaffold_riverpod/modules/customer/edit/widgets/customer_text_input.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/buttons/back_icon_button.dart';

class CustomerEditAppBar extends AppBar {
  CustomerEditAppBar(
    this.customer, {
    super.key,
  });

  final Customer? customer;

  @override
  State<CustomerEditAppBar> createState() => _CustomerEditAppBarState();
}

class _CustomerEditAppBarState extends State<CustomerEditAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: Consumer(builder: (context, ref, child) {
        final logic = ref.read(CustomerEditLogic.provider(widget.customer?.id).notifier);
        return BackIconButton(
          onTap: logic.onCancel,
        );
      }),
      title: Consumer(
        builder: (context, ref, child) {
          final logic = ref.read(CustomerEditLogic.provider(widget.customer?.id).notifier);
          final stateCustomer =
              ref.watch(CustomerEditLogic.provider(widget.customer?.id).select((s) => s.customer)) ?? widget.customer;
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Hero(
                  tag: 'customer_first_name_${widget.customer?.id}',
                  child: CustomerTextInput(
                    initialValue: stateCustomer?.firstName,
                    validator: logic.firstNameValidator,
                    focusNode: logic.firstNameFocusNode,
                    controller: logic.firstNameController,
                    textStyle: context.textTheme.headline2,
                    hintText: context.translation.firstName,
                    hintTextStyle: context.textTheme.headline5,
                    onSubmit: (_) => logic.lastNameFocusNode.requestFocus(),
                  ),
                ),
              ),
              Expanded(
                child: Hero(
                  tag: 'customer_last_name_${widget.customer?.id}',
                  child: CustomerTextInput(
                    initialValue: stateCustomer?.lastName,
                    validator: logic.lastNameValidator,
                    focusNode: logic.lastNameFocusNode,
                    controller: logic.lastNameController,
                    textStyle: context.textTheme.headline2,
                    hintText: context.translation.lastName,
                    hintTextStyle: context.textTheme.headline5,
                    onSubmit: (_) => logic.phoneFocusNode.requestFocus(),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
