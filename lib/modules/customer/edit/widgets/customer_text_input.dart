import 'package:flutter/material.dart';
import 'package:scaffold_riverpod/config/theme/colors.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/misc/shake_on_trigger.dart';

class CustomerTextInput extends StatefulWidget {
  const CustomerTextInput({
    super.key,
    this.focusNode,
    this.controller,
    this.initialValue,
    required this.hintText,
    this.height,
    this.textStyle,
    this.hintTextStyle,
    this.onSubmit,
    this.validator,
  });

  final FocusNode? focusNode;
  final TextEditingController? controller;
  final String hintText;
  final String? initialValue;
  final double? height;
  final TextStyle? textStyle;
  final TextStyle? hintTextStyle;
  final void Function(String)? onSubmit;
  final String? Function(String?)? validator;

  @override
  State<CustomerTextInput> createState() => _CustomerTextInputState();
}

class _CustomerTextInputState extends State<CustomerTextInput> {
  final shakeKey = GlobalKey<ShakeOnTriggerState>();
  final inputKey = GlobalKey<FormFieldState>();

  @override
  Widget build(BuildContext context) {
    Widget input = TextFormField(
      key: inputKey,
      initialValue: widget.controller == null ? widget.initialValue : null,
      validator: widget.validator != null
          ? (value) {
              final error = widget.validator!(value);
              if (error != null) {
                shakeKey.currentState?.animate();
                setState(() {});
              }
              return error;
            }
          : null,
      focusNode: widget.focusNode,
      controller: widget.controller,
      onFieldSubmitted: widget.onSubmit,
      style: widget.textStyle ?? context.textTheme.bodyText1,
      decoration: InputDecoration(
        isDense: true,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: primaryLightestColor, width: 1),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: primaryColor, width: 1),
        ),
        contentPadding: const EdgeInsets.only(bottom: $Padding.xxxs),
        hintText: widget.hintText,
        hintStyle: widget.hintTextStyle ?? context.textTheme.bodyText2,
        errorStyle: const TextStyle(fontSize: $Font.xs),
      ),
    );
    if (widget.validator != null) {
      input = ShakeOnTrigger(
        key: shakeKey,
        child: input,
      );
    }
    final height = (widget.height ?? $InputHeight.xs) + (inputKey.currentState?.hasError ?? false ? $Font.xs : 0);
    return SizedBox(
      height: height,
      child: input,
    );
  }
}
