import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/edit/customer_edit_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/layout/rounded_material.dart';

class CustomerCancelConfirmationModal extends ConsumerWidget {
  const CustomerCancelConfirmationModal(
    this.customer, {
    super.key,
  });

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: $Padding.m),
        child: RoundedContainer(
          color: context.theme.snackBarTheme.backgroundColor?.withOpacity(0.95),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: $Padding.s),
                  child: Text(context.translation.youWillLooseUnsavedModifications, style: context.textTheme.subtitle2),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: $Padding.s),
                      child: Column(
                        children: [
                          RoundedContainer(
                            onTap: () {
                              logic.cancel();
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.backspace_rounded,
                              size: $Icon.m,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: $Padding.xs),
                            child: Text(context.translation.leave),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: $Padding.s),
                      child: Column(
                        children: [
                          RoundedContainer(
                            onTap: () {
                              logic.saveCustomer();
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.save_rounded,
                              size: $Icon.m,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: $Padding.xs),
                            child: Text(context.translation.save),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: $Padding.s),
                      child: Column(
                        children: [
                          RoundedContainer(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.edit_rounded,
                              size: $Icon.m,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: $Padding.xs),
                            child: Text(context.translation.edit),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
