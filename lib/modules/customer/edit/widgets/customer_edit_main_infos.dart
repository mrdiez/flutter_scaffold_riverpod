import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/edit/customer_edit_logic.dart';
import 'package:scaffold_riverpod/modules/customer/edit/widgets/customer_text_input.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/layout/rounded_material.dart';

class CustomerEditMainInfos extends StatelessWidget {
  const CustomerEditMainInfos(this.customer, {super.key});

  final Customer? customer;

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      heroTag: 'customer_main_info_container_${customer?.id}',
      innerPadding: const EdgeInsets.all($Padding.s),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Icon(Icons.phone_rounded, color: context.primaryColor, size: $Icon.s),
                $Gap.s,
                _CustomerPhoneInput(customer),
              ],
            ),
            $Gap.xxxs,
            Row(
              children: [
                Icon(Icons.email_rounded, color: context.primaryColor, size: $Icon.s),
                $Gap.s,
                _CustomerEmailInput(customer),
              ],
            ),
            $Gap.xxxs,
            Row(
              children: [
                Icon(Icons.place_rounded, color: context.primaryColor, size: $Icon.s),
                $Gap.s,
                Expanded(
                  child: Column(
                    children: [
                      _CustomerAddress1Input(customer),
                      _CustomerAddress2Input(customer),
                      Row(
                        children: [
                          _CustomerPostalCodeInput(customer),
                          $Gap.s,
                          _CustomerCityInput(customer),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _CustomerPhoneInput extends ConsumerWidget {
  const _CustomerPhoneInput(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: $Padding.xxxs, bottom: $Padding.xxxs),
        child: CustomerTextInput(
          initialValue: customer?.phone,
          validator: logic.phoneValidator,
          focusNode: logic.phoneFocusNode,
          controller: logic.phoneController,
          hintText: context.translation.phone,
          textStyle: context.textTheme.bodyText2,
          onSubmit: (_) => logic.emailFocusNode.requestFocus(),
          height: $Icon.s,
        ),
      ),
    );
  }
}

class _CustomerEmailInput extends ConsumerWidget {
  const _CustomerEmailInput(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: $Padding.xxxs),
        child: CustomerTextInput(
          initialValue: customer?.email,
          validator: logic.emailValidator,
          focusNode: logic.emailFocusNode,
          controller: logic.emailController,
          hintText: context.translation.email,
          textStyle: context.textTheme.bodyText2,
          onSubmit: (_) => logic.address1FocusNode.requestFocus(),
          height: $Icon.s,
        ),
      ),
    );
  }
}

class _CustomerAddress1Input extends ConsumerWidget {
  const _CustomerAddress1Input(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Padding(
      padding: const EdgeInsets.only(top: $Padding.xxxs),
      child: CustomerTextInput(
        initialValue: customer?.address1,
        focusNode: logic.address1FocusNode,
        controller: logic.address1Controller,
        hintText: '${context.translation.address} 1',
        textStyle: context.textTheme.bodyText2,
        onSubmit: (_) => logic.address2FocusNode.requestFocus(),
        height: $Icon.s,
      ),
    );
  }
}

class _CustomerAddress2Input extends ConsumerWidget {
  const _CustomerAddress2Input(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return CustomerTextInput(
      initialValue: customer?.address2,
      focusNode: logic.address2FocusNode,
      controller: logic.address2Controller,
      hintText: '${context.translation.address} 2',
      textStyle: context.textTheme.bodyText2,
      onSubmit: (_) => logic.postalCodeFocusNode.requestFocus(),
      height: $Icon.s,
    );
  }
}

class _CustomerPostalCodeInput extends ConsumerWidget {
  const _CustomerPostalCodeInput(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Expanded(
      flex: 2,
      child: CustomerTextInput(
        initialValue: customer?.postalCode,
        validator: logic.postalCodeValidator,
        focusNode: logic.postalCodeFocusNode,
        controller: logic.postalCodeController,
        hintText: context.translation.postalCode,
        textStyle: context.textTheme.bodyText2,
        onSubmit: (_) => logic.cityFocusNode.requestFocus(),
        height: $Icon.s,
      ),
    );
  }
}

class _CustomerCityInput extends ConsumerWidget {
  const _CustomerCityInput(this.customer);

  final Customer? customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    return Expanded(
      flex: 3,
      child: CustomerTextInput(
        initialValue: customer?.city,
        focusNode: logic.cityFocusNode,
        controller: logic.cityController,
        hintText: context.translation.city,
        textStyle: context.textTheme.bodyText2,
        height: $Icon.s,
      ),
    );
  }
}
