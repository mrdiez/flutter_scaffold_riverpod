import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:scaffold_riverpod/config/translations/gen/l10n.dart';
import 'package:scaffold_riverpod/data/access/customer_repository.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/customer_details_logic.dart';
import 'package:scaffold_riverpod/utils/form.dart';
import 'package:scaffold_riverpod/utils/number.dart';
import 'package:scaffold_riverpod/utils/string.dart';

import 'customer_edit_view.dart';

part 'customer_edit_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerEditEvent with _$CustomerEditEvent {
  static StreamController<CustomerEditEvent>? streamController;

  static final provider = StreamProvider<CustomerEditEvent>((ref) {
    streamController = StreamController<CustomerEditEvent>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });

  const factory CustomerEditEvent.customerSaved() = CustomerEditEventCustomerSaved;
  const factory CustomerEditEvent.cancel() = CustomerEditEventCancel;
  const factory CustomerEditEvent.toggleCancelConfirmationModal() = CustomerEditEventToggleCancelConfirmationModal;
  const factory CustomerEditEvent.unknownError([String? message]) = CustomerEditEventUnknownError;
}

class CustomerEditLogic extends StateNotifier<CustomerEditState> {
  static final provider =
      StateNotifierProvider.autoDispose.family<CustomerEditLogic, CustomerEditState, int?>((ref, customerId) {
    return CustomerEditLogic(
      ref: ref,
      initialState: const CustomerEditState(),
      customerRepository: ref.watch(CustomerRepository.provider),
      customerId: customerId,
    );
  });

  CustomerEditLogic({
    required this.ref,
    required CustomerEditState initialState,
    required this.customerRepository,
    required this.customerId,
  })  : _keepAliveLink = ref.keepAlive(),
        super(initialState) {
    if (customerId.isNotNull) {
      _init(customerId!);
    } else {
      customerRepository.insert(Customer.empty()).then(_init);
    }
  }

  final AutoDisposeRef ref;
  final KeepAliveLink _keepAliveLink;
  final int? customerId;
  late final Customer _customer;
  final CustomerRepository customerRepository;

  TextEditingController? firstNameController;
  TextEditingController? lastNameController;
  TextEditingController? phoneController;
  TextEditingController? emailController;
  TextEditingController? address1Controller;
  TextEditingController? address2Controller;
  TextEditingController? postalCodeController;
  TextEditingController? cityController;
  final firstNameFocusNode = FocusNode();
  final lastNameFocusNode = FocusNode();
  final phoneFocusNode = FocusNode();
  final emailFocusNode = FocusNode();
  final address1FocusNode = FocusNode();
  final address2FocusNode = FocusNode();
  final postalCodeFocusNode = FocusNode();
  final cityFocusNode = FocusNode();

  final Map<TextEditingController, void Function(String)> _customerPropertySetterByInputController = {};

  final formKey = GlobalKey<FormState>();
  FormState get _formState => formKey.currentState!;

  void _init(int customerId) {
    customerRepository.get(customerId).then(
      (customer) {
        if (customer == null) throw ('Customer not found');
        _customer = customer;
        _mapAllCustomerPropertySettersByInputController();
        state = state.copyWith(customer: customer);
      },
      onError: (error, stackTrace) {
        state = state.copyWith(error: true);
        CustomerDetailsEvent.streamController!.add(CustomerDetailsEvent.unknownError(error));
      },
    );
  }

  void onEdit() {
    int i = 0;
    String text = '';
    for (final inputController in _customerPropertySetterByInputController.keys) {
      text += '$i ${inputController.text}\n';
      i++;
    }
    debugPrint(text);
  }

  void _mapAllCustomerPropertySettersByInputController() {
    firstNameController = _mapCustomerPropertySetterByInputController(
      _customer.firstName,
      (e) => _customer.firstName = e,
    );
    lastNameController = _mapCustomerPropertySetterByInputController(
      _customer.lastName,
      (e) => _customer.lastName = e,
    );
    phoneController = _mapCustomerPropertySetterByInputController(
      _customer.phone,
      (e) => _customer.phone = e,
    );
    emailController = _mapCustomerPropertySetterByInputController(
      _customer.email,
      (e) => _customer.email = e,
    );
    address1Controller = _mapCustomerPropertySetterByInputController(
      _customer.address1,
      (e) => _customer.address1 = e,
    );
    address2Controller = _mapCustomerPropertySetterByInputController(
      _customer.address2,
      (e) => _customer.address2 = e,
    );
    postalCodeController = _mapCustomerPropertySetterByInputController(
      _customer.postalCode,
      (e) => _customer.postalCode = e,
    );
    cityController = _mapCustomerPropertySetterByInputController(
      _customer.city,
      (e) => _customer.city = e,
    );
  }

  TextEditingController _mapCustomerPropertySetterByInputController(String? getter, void Function(String) setter) {
    final result = TextEditingController(text: getter);
    _customerPropertySetterByInputController[result] = setter;
    return result;
  }

  void _setCustomerPropertyByInputController(TextEditingController inputController) =>
      _customerPropertySetterByInputController[inputController]!(inputController.text);

  void _trimInputValues() {
    for (final inputController in _customerPropertySetterByInputController.keys) {
      inputController.setValue(inputController.text.trim());
    }
  }

  List<TextEditingController> get _editedInputControllers {
    List<TextEditingController> result = [];
    if (firstNameController!.value.text != _customer.firstName) {
      result.add(firstNameController!);
    }
    if (lastNameController!.text != _customer.lastName) {
      result.add(lastNameController!);
    }
    if (phoneController!.text != (_customer.phone ?? '')) result.add(phoneController!);
    if (emailController!.text != (_customer.email ?? '')) result.add(emailController!);
    if (address1Controller!.text != (_customer.address1 ?? '')) result.add(address1Controller!);
    if (address2Controller!.text != (_customer.address2 ?? '')) result.add(address2Controller!);
    if (postalCodeController!.text != (_customer.postalCode ?? '')) result.add(postalCodeController!);
    if (cityController!.text != (_customer.city ?? '')) result.add(cityController!);
    return result;
  }

  Future<void> saveCustomer() async {
    _trimInputValues();
    if (!_formState.validate()) return;
    for (final inputController in _editedInputControllers) {
      _setCustomerPropertyByInputController(inputController);
    }
    await customerRepository.update(_customer);
    _keepAliveLink.close();
    CustomerEditEvent.streamController?.add(const CustomerEditEvent.customerSaved());
  }

  Future<void> onCancel() async {
    _trimInputValues();
    if (_editedInputControllers.isNotEmpty) {
      CustomerEditEvent.streamController?.add(const CustomerEditEvent.toggleCancelConfirmationModal());
    } else {
      cancel();
    }
  }

  Future<void> cancel() async {
    if (customerId.isNull) {
      await customerRepository.delete(_customer.id);
    }
    _keepAliveLink.close();
    CustomerEditEvent.streamController?.add(const CustomerEditEvent.cancel());
  }

  String? firstNameValidator(String? value) {
    if (value.isNullOrBlank) return Translation.current.firstNameRequired;
    return null;
  }

  String? lastNameValidator(String? value) {
    if (value.isNullOrBlank) return Translation.current.lastNameRequired;
    return null;
  }

  String? phoneValidator(String? value) {
    if (value.isNullOrBlank) return null;
    if (!value!.isPhone) return Translation.current.notValidPhone;
    return null;
  }

  String? emailValidator(String? value) {
    if (value.isNullOrBlank) return null;
    if (!value!.isEmail) return Translation.current.notValidEmail;
    return null;
  }

  String? postalCodeValidator(String? value) {
    if (value.isNullOrBlank) return null;
    if (!value!.isPostalCode) return Translation.current.notValidPostalCode;
    return null;
  }
}
