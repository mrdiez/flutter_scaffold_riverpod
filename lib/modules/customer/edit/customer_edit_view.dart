import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/edit/widgets/customer_cancel_confirmation_modal.dart';
import 'package:scaffold_riverpod/modules/customer/edit/widgets/customer_edit_app_bar.dart';
import 'package:scaffold_riverpod/modules/customer/edit/widgets/customer_edit_main_infos.dart';
import 'package:scaffold_riverpod/services/common_event.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/texts/section_title.dart';

import 'customer_edit_logic.dart';

part 'customer_edit_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerEditState with _$CustomerEditState {
  const factory CustomerEditState({
    Customer? customer,
    @Default(false) bool error,
  }) = _CustomerEditState;

  const CustomerEditState._();
}

class CustomerEditView extends ConsumerWidget {
  const CustomerEditView(
    this.customer, {
    super.key,
  });

  final Customer? customer;

  void onEvent(CustomerEditEvent e, BuildContext context) {
    if (e is CustomerEditEventCustomerSaved || e is CustomerEditEventCancel) {
      context.pop();
    } else if (e is CustomerEditEventToggleCancelConfirmationModal) {
      context.showModal(CustomerCancelConfirmationModal(customer));
    } else if (e is CustomerEditEventUnknownError && e.message != null) {
      debugPrint(e.message);
    }
  }

  void onCommonEvent(CommonEvent e, CustomerEditLogic logic) {
    if (e is CommonEventPop) {
      logic.onCancel();
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerEditLogic.provider(customer?.id).notifier);
    ref.listen<AsyncValue<CustomerEditEvent>>(
        CustomerEditEvent.provider, (previous, next) => onEvent(next.value!, context));
    ref.listen<AsyncValue<CommonEvent>>(CommonEvent.provider, (previous, next) => onCommonEvent(next.value!, logic));
    return Form(
      key: logic.formKey,
      onChanged: logic.onEdit,
      child: Scaffold(
        appBar: CustomerEditAppBar(customer),
        floatingActionButton: FloatingActionButton(
          onPressed: logic.saveCustomer,
          child: const Icon(Icons.save_rounded, size: $Icon.s),
        ),
        body: Padding(
          padding: const EdgeInsets.all($Padding.m),
          child: Consumer(builder: (context, ref, child) {
            final error = ref.watch(CustomerEditLogic.provider(customer?.id).select((s) => s.error));
            return error
                ? Center(child: Text(context.translation.unknownError))
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SectionTitle(text: context.translation.mainInfo, textAsHeroTag: true),
                      $Gap.s,
                      Consumer(
                        builder: (context, ref, child) {
                          final stateCustomer =
                              ref.watch(CustomerEditLogic.provider(customer?.id).select((s) => s.customer)) ?? customer;
                          return CustomerEditMainInfos(stateCustomer);
                        },
                      ),
                    ],
                  );
          }),
        ),
      ),
    );
  }
}
