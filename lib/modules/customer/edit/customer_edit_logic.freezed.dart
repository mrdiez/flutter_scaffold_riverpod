// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'customer_edit_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomerEditEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() customerSaved,
    required TResult Function() cancel,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? customerSaved,
    TResult? Function()? cancel,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? customerSaved,
    TResult Function()? cancel,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerEditEventCustomerSaved value)
        customerSaved,
    required TResult Function(CustomerEditEventCancel value) cancel,
    required TResult Function(
            CustomerEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(CustomerEditEventUnknownError value) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult? Function(CustomerEditEventCancel value)? cancel,
    TResult? Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(CustomerEditEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult Function(CustomerEditEventCancel value)? cancel,
    TResult Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(CustomerEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerEditEventCopyWith<$Res> {
  factory $CustomerEditEventCopyWith(
          CustomerEditEvent value, $Res Function(CustomerEditEvent) then) =
      _$CustomerEditEventCopyWithImpl<$Res, CustomerEditEvent>;
}

/// @nodoc
class _$CustomerEditEventCopyWithImpl<$Res, $Val extends CustomerEditEvent>
    implements $CustomerEditEventCopyWith<$Res> {
  _$CustomerEditEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CustomerEditEventCustomerSavedCopyWith<$Res> {
  factory _$$CustomerEditEventCustomerSavedCopyWith(
          _$CustomerEditEventCustomerSaved value,
          $Res Function(_$CustomerEditEventCustomerSaved) then) =
      __$$CustomerEditEventCustomerSavedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerEditEventCustomerSavedCopyWithImpl<$Res>
    extends _$CustomerEditEventCopyWithImpl<$Res,
        _$CustomerEditEventCustomerSaved>
    implements _$$CustomerEditEventCustomerSavedCopyWith<$Res> {
  __$$CustomerEditEventCustomerSavedCopyWithImpl(
      _$CustomerEditEventCustomerSaved _value,
      $Res Function(_$CustomerEditEventCustomerSaved) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerEditEventCustomerSaved
    with DiagnosticableTreeMixin
    implements CustomerEditEventCustomerSaved {
  const _$CustomerEditEventCustomerSaved();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerEditEvent.customerSaved()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'CustomerEditEvent.customerSaved'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerEditEventCustomerSaved);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() customerSaved,
    required TResult Function() cancel,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return customerSaved();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? customerSaved,
    TResult? Function()? cancel,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return customerSaved?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? customerSaved,
    TResult Function()? cancel,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (customerSaved != null) {
      return customerSaved();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerEditEventCustomerSaved value)
        customerSaved,
    required TResult Function(CustomerEditEventCancel value) cancel,
    required TResult Function(
            CustomerEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(CustomerEditEventUnknownError value) unknownError,
  }) {
    return customerSaved(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult? Function(CustomerEditEventCancel value)? cancel,
    TResult? Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(CustomerEditEventUnknownError value)? unknownError,
  }) {
    return customerSaved?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult Function(CustomerEditEventCancel value)? cancel,
    TResult Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(CustomerEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (customerSaved != null) {
      return customerSaved(this);
    }
    return orElse();
  }
}

abstract class CustomerEditEventCustomerSaved implements CustomerEditEvent {
  const factory CustomerEditEventCustomerSaved() =
      _$CustomerEditEventCustomerSaved;
}

/// @nodoc
abstract class _$$CustomerEditEventCancelCopyWith<$Res> {
  factory _$$CustomerEditEventCancelCopyWith(_$CustomerEditEventCancel value,
          $Res Function(_$CustomerEditEventCancel) then) =
      __$$CustomerEditEventCancelCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerEditEventCancelCopyWithImpl<$Res>
    extends _$CustomerEditEventCopyWithImpl<$Res, _$CustomerEditEventCancel>
    implements _$$CustomerEditEventCancelCopyWith<$Res> {
  __$$CustomerEditEventCancelCopyWithImpl(_$CustomerEditEventCancel _value,
      $Res Function(_$CustomerEditEventCancel) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerEditEventCancel
    with DiagnosticableTreeMixin
    implements CustomerEditEventCancel {
  const _$CustomerEditEventCancel();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerEditEvent.cancel()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'CustomerEditEvent.cancel'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerEditEventCancel);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() customerSaved,
    required TResult Function() cancel,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return cancel();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? customerSaved,
    TResult? Function()? cancel,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return cancel?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? customerSaved,
    TResult Function()? cancel,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (cancel != null) {
      return cancel();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerEditEventCustomerSaved value)
        customerSaved,
    required TResult Function(CustomerEditEventCancel value) cancel,
    required TResult Function(
            CustomerEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(CustomerEditEventUnknownError value) unknownError,
  }) {
    return cancel(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult? Function(CustomerEditEventCancel value)? cancel,
    TResult? Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(CustomerEditEventUnknownError value)? unknownError,
  }) {
    return cancel?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult Function(CustomerEditEventCancel value)? cancel,
    TResult Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(CustomerEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (cancel != null) {
      return cancel(this);
    }
    return orElse();
  }
}

abstract class CustomerEditEventCancel implements CustomerEditEvent {
  const factory CustomerEditEventCancel() = _$CustomerEditEventCancel;
}

/// @nodoc
abstract class _$$CustomerEditEventToggleCancelConfirmationModalCopyWith<$Res> {
  factory _$$CustomerEditEventToggleCancelConfirmationModalCopyWith(
          _$CustomerEditEventToggleCancelConfirmationModal value,
          $Res Function(_$CustomerEditEventToggleCancelConfirmationModal)
              then) =
      __$$CustomerEditEventToggleCancelConfirmationModalCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerEditEventToggleCancelConfirmationModalCopyWithImpl<$Res>
    extends _$CustomerEditEventCopyWithImpl<$Res,
        _$CustomerEditEventToggleCancelConfirmationModal>
    implements _$$CustomerEditEventToggleCancelConfirmationModalCopyWith<$Res> {
  __$$CustomerEditEventToggleCancelConfirmationModalCopyWithImpl(
      _$CustomerEditEventToggleCancelConfirmationModal _value,
      $Res Function(_$CustomerEditEventToggleCancelConfirmationModal) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerEditEventToggleCancelConfirmationModal
    with DiagnosticableTreeMixin
    implements CustomerEditEventToggleCancelConfirmationModal {
  const _$CustomerEditEventToggleCancelConfirmationModal();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerEditEvent.toggleCancelConfirmationModal()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'CustomerEditEvent.toggleCancelConfirmationModal'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerEditEventToggleCancelConfirmationModal);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() customerSaved,
    required TResult Function() cancel,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return toggleCancelConfirmationModal();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? customerSaved,
    TResult? Function()? cancel,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return toggleCancelConfirmationModal?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? customerSaved,
    TResult Function()? cancel,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (toggleCancelConfirmationModal != null) {
      return toggleCancelConfirmationModal();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerEditEventCustomerSaved value)
        customerSaved,
    required TResult Function(CustomerEditEventCancel value) cancel,
    required TResult Function(
            CustomerEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(CustomerEditEventUnknownError value) unknownError,
  }) {
    return toggleCancelConfirmationModal(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult? Function(CustomerEditEventCancel value)? cancel,
    TResult? Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(CustomerEditEventUnknownError value)? unknownError,
  }) {
    return toggleCancelConfirmationModal?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult Function(CustomerEditEventCancel value)? cancel,
    TResult Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(CustomerEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (toggleCancelConfirmationModal != null) {
      return toggleCancelConfirmationModal(this);
    }
    return orElse();
  }
}

abstract class CustomerEditEventToggleCancelConfirmationModal
    implements CustomerEditEvent {
  const factory CustomerEditEventToggleCancelConfirmationModal() =
      _$CustomerEditEventToggleCancelConfirmationModal;
}

/// @nodoc
abstract class _$$CustomerEditEventUnknownErrorCopyWith<$Res> {
  factory _$$CustomerEditEventUnknownErrorCopyWith(
          _$CustomerEditEventUnknownError value,
          $Res Function(_$CustomerEditEventUnknownError) then) =
      __$$CustomerEditEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$CustomerEditEventUnknownErrorCopyWithImpl<$Res>
    extends _$CustomerEditEventCopyWithImpl<$Res,
        _$CustomerEditEventUnknownError>
    implements _$$CustomerEditEventUnknownErrorCopyWith<$Res> {
  __$$CustomerEditEventUnknownErrorCopyWithImpl(
      _$CustomerEditEventUnknownError _value,
      $Res Function(_$CustomerEditEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$CustomerEditEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$CustomerEditEventUnknownError
    with DiagnosticableTreeMixin
    implements CustomerEditEventUnknownError {
  const _$CustomerEditEventUnknownError([this.message]);

  @override
  final String? message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerEditEvent.unknownError(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CustomerEditEvent.unknownError'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerEditEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerEditEventUnknownErrorCopyWith<_$CustomerEditEventUnknownError>
      get copyWith => __$$CustomerEditEventUnknownErrorCopyWithImpl<
          _$CustomerEditEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() customerSaved,
    required TResult Function() cancel,
    required TResult Function() toggleCancelConfirmationModal,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? customerSaved,
    TResult? Function()? cancel,
    TResult? Function()? toggleCancelConfirmationModal,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? customerSaved,
    TResult Function()? cancel,
    TResult Function()? toggleCancelConfirmationModal,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerEditEventCustomerSaved value)
        customerSaved,
    required TResult Function(CustomerEditEventCancel value) cancel,
    required TResult Function(
            CustomerEditEventToggleCancelConfirmationModal value)
        toggleCancelConfirmationModal,
    required TResult Function(CustomerEditEventUnknownError value) unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult? Function(CustomerEditEventCancel value)? cancel,
    TResult? Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult? Function(CustomerEditEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerEditEventCustomerSaved value)? customerSaved,
    TResult Function(CustomerEditEventCancel value)? cancel,
    TResult Function(CustomerEditEventToggleCancelConfirmationModal value)?
        toggleCancelConfirmationModal,
    TResult Function(CustomerEditEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class CustomerEditEventUnknownError implements CustomerEditEvent {
  const factory CustomerEditEventUnknownError([final String? message]) =
      _$CustomerEditEventUnknownError;

  String? get message;
  @JsonKey(ignore: true)
  _$$CustomerEditEventUnknownErrorCopyWith<_$CustomerEditEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
