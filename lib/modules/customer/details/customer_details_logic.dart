import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:scaffold_riverpod/config/translations/gen/l10n.dart';
import 'package:scaffold_riverpod/data/access/customer_repository.dart';
import 'package:url_launcher/url_launcher.dart';

import 'customer_details_view.dart';

part 'customer_details_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerDetailsEvent with _$CustomerDetailsEvent {
  static StreamController<CustomerDetailsEvent>? streamController;

  static final provider = StreamProvider<CustomerDetailsEvent>((ref) {
    streamController = StreamController<CustomerDetailsEvent>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });
  const factory CustomerDetailsEvent.togglePhoneAction() = CustomerDetailsEventTogglePhoneAction;
  const factory CustomerDetailsEvent.unknownError([String? message]) = CustomerDetailsEventUnknownError;
}

class CustomerDetailsLogic extends StateNotifier<CustomerDetailsState> {
  static final provider =
      StateNotifierProvider.family<CustomerDetailsLogic, CustomerDetailsState, int>((ref, customerId) {
    return CustomerDetailsLogic(
      initialState: const CustomerDetailsState(),
      ref: ref,
      customerRepository: ref.watch(CustomerRepository.provider),
      customerId: customerId,
    );
  });

  CustomerDetailsLogic({
    required CustomerDetailsState initialState,
    required this.ref,
    required this.customerRepository,
    required this.customerId,
  }) : super(initialState) {
    customerRepository.watch(customerId).listen(
      (customer) {
        if (customer == null) throw ('Customer not found');
        state = state.copyWith(customer: customer);
      },
      onError: (error, stackTrace) {
        state = state.copyWith(error: true);
        CustomerDetailsEvent.streamController!.add(CustomerDetailsEvent.unknownError(error));
      },
    );
  }

  final int customerId;
  final Ref ref;
  final CustomerRepository customerRepository;
  String? _phone;

  void onPhoneAction(String phone) {
    _phone = phone;
    CustomerDetailsEvent.streamController!.add(const CustomerDetailsEvent.togglePhoneAction());
  }

  void call() {
    launchUrl(Uri(scheme: 'tel', path: _phone));
  }

  final _defaultSms = <String, String>{
    'body': '${Translation.current.hello}\n\n',
  };
  void sms() {
    launchUrl(Uri(scheme: 'sms', path: _phone, queryParameters: _defaultSms));
  }

  final _defaultEmail = <String, String>{
    'subject': '',
    'body': '${Translation.current.hello}\n\n',
  }
      .entries
      .map((MapEntry<String, String> e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
  void mail(String email) {
    launchUrl(Uri(scheme: 'mailto', path: email, query: _defaultEmail));
  }

  void itinerary(String address) {
    MapsLauncher.launchQuery(address);
  }
}
