// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'customer_details_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomerDetailsState {
  Customer? get customer => throw _privateConstructorUsedError;
  bool get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerDetailsStateCopyWith<CustomerDetailsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerDetailsStateCopyWith<$Res> {
  factory $CustomerDetailsStateCopyWith(CustomerDetailsState value,
          $Res Function(CustomerDetailsState) then) =
      _$CustomerDetailsStateCopyWithImpl<$Res, CustomerDetailsState>;
  @useResult
  $Res call({Customer? customer, bool error});
}

/// @nodoc
class _$CustomerDetailsStateCopyWithImpl<$Res,
        $Val extends CustomerDetailsState>
    implements $CustomerDetailsStateCopyWith<$Res> {
  _$CustomerDetailsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customer = freezed,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      customer: freezed == customer
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as Customer?,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CustomerDetailsStateCopyWith<$Res>
    implements $CustomerDetailsStateCopyWith<$Res> {
  factory _$$_CustomerDetailsStateCopyWith(_$_CustomerDetailsState value,
          $Res Function(_$_CustomerDetailsState) then) =
      __$$_CustomerDetailsStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Customer? customer, bool error});
}

/// @nodoc
class __$$_CustomerDetailsStateCopyWithImpl<$Res>
    extends _$CustomerDetailsStateCopyWithImpl<$Res, _$_CustomerDetailsState>
    implements _$$_CustomerDetailsStateCopyWith<$Res> {
  __$$_CustomerDetailsStateCopyWithImpl(_$_CustomerDetailsState _value,
      $Res Function(_$_CustomerDetailsState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? customer = freezed,
    Object? error = null,
  }) {
    return _then(_$_CustomerDetailsState(
      customer: freezed == customer
          ? _value.customer
          : customer // ignore: cast_nullable_to_non_nullable
              as Customer?,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_CustomerDetailsState extends _CustomerDetailsState {
  const _$_CustomerDetailsState({this.customer, this.error = false})
      : super._();

  @override
  final Customer? customer;
  @override
  @JsonKey()
  final bool error;

  @override
  String toString() {
    return 'CustomerDetailsState(customer: $customer, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CustomerDetailsState &&
            (identical(other.customer, customer) ||
                other.customer == customer) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, customer, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CustomerDetailsStateCopyWith<_$_CustomerDetailsState> get copyWith =>
      __$$_CustomerDetailsStateCopyWithImpl<_$_CustomerDetailsState>(
          this, _$identity);
}

abstract class _CustomerDetailsState extends CustomerDetailsState {
  const factory _CustomerDetailsState(
      {final Customer? customer, final bool error}) = _$_CustomerDetailsState;
  const _CustomerDetailsState._() : super._();

  @override
  Customer? get customer;
  @override
  bool get error;
  @override
  @JsonKey(ignore: true)
  _$$_CustomerDetailsStateCopyWith<_$_CustomerDetailsState> get copyWith =>
      throw _privateConstructorUsedError;
}
