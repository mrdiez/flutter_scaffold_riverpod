// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'customer_details_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomerDetailsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() togglePhoneAction,
    required TResult Function(String? message) unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? togglePhoneAction,
    TResult? Function(String? message)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? togglePhoneAction,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerDetailsEventTogglePhoneAction value)
        togglePhoneAction,
    required TResult Function(CustomerDetailsEventUnknownError value)
        unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult? Function(CustomerDetailsEventUnknownError value)? unknownError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult Function(CustomerDetailsEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerDetailsEventCopyWith<$Res> {
  factory $CustomerDetailsEventCopyWith(CustomerDetailsEvent value,
          $Res Function(CustomerDetailsEvent) then) =
      _$CustomerDetailsEventCopyWithImpl<$Res, CustomerDetailsEvent>;
}

/// @nodoc
class _$CustomerDetailsEventCopyWithImpl<$Res,
        $Val extends CustomerDetailsEvent>
    implements $CustomerDetailsEventCopyWith<$Res> {
  _$CustomerDetailsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CustomerDetailsEventTogglePhoneActionCopyWith<$Res> {
  factory _$$CustomerDetailsEventTogglePhoneActionCopyWith(
          _$CustomerDetailsEventTogglePhoneAction value,
          $Res Function(_$CustomerDetailsEventTogglePhoneAction) then) =
      __$$CustomerDetailsEventTogglePhoneActionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CustomerDetailsEventTogglePhoneActionCopyWithImpl<$Res>
    extends _$CustomerDetailsEventCopyWithImpl<$Res,
        _$CustomerDetailsEventTogglePhoneAction>
    implements _$$CustomerDetailsEventTogglePhoneActionCopyWith<$Res> {
  __$$CustomerDetailsEventTogglePhoneActionCopyWithImpl(
      _$CustomerDetailsEventTogglePhoneAction _value,
      $Res Function(_$CustomerDetailsEventTogglePhoneAction) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CustomerDetailsEventTogglePhoneAction
    with DiagnosticableTreeMixin
    implements CustomerDetailsEventTogglePhoneAction {
  const _$CustomerDetailsEventTogglePhoneAction();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerDetailsEvent.togglePhoneAction()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'CustomerDetailsEvent.togglePhoneAction'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerDetailsEventTogglePhoneAction);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() togglePhoneAction,
    required TResult Function(String? message) unknownError,
  }) {
    return togglePhoneAction();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? togglePhoneAction,
    TResult? Function(String? message)? unknownError,
  }) {
    return togglePhoneAction?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? togglePhoneAction,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (togglePhoneAction != null) {
      return togglePhoneAction();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerDetailsEventTogglePhoneAction value)
        togglePhoneAction,
    required TResult Function(CustomerDetailsEventUnknownError value)
        unknownError,
  }) {
    return togglePhoneAction(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult? Function(CustomerDetailsEventUnknownError value)? unknownError,
  }) {
    return togglePhoneAction?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult Function(CustomerDetailsEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (togglePhoneAction != null) {
      return togglePhoneAction(this);
    }
    return orElse();
  }
}

abstract class CustomerDetailsEventTogglePhoneAction
    implements CustomerDetailsEvent {
  const factory CustomerDetailsEventTogglePhoneAction() =
      _$CustomerDetailsEventTogglePhoneAction;
}

/// @nodoc
abstract class _$$CustomerDetailsEventUnknownErrorCopyWith<$Res> {
  factory _$$CustomerDetailsEventUnknownErrorCopyWith(
          _$CustomerDetailsEventUnknownError value,
          $Res Function(_$CustomerDetailsEventUnknownError) then) =
      __$$CustomerDetailsEventUnknownErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$CustomerDetailsEventUnknownErrorCopyWithImpl<$Res>
    extends _$CustomerDetailsEventCopyWithImpl<$Res,
        _$CustomerDetailsEventUnknownError>
    implements _$$CustomerDetailsEventUnknownErrorCopyWith<$Res> {
  __$$CustomerDetailsEventUnknownErrorCopyWithImpl(
      _$CustomerDetailsEventUnknownError _value,
      $Res Function(_$CustomerDetailsEventUnknownError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$CustomerDetailsEventUnknownError(
      freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$CustomerDetailsEventUnknownError
    with DiagnosticableTreeMixin
    implements CustomerDetailsEventUnknownError {
  const _$CustomerDetailsEventUnknownError([this.message]);

  @override
  final String? message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CustomerDetailsEvent.unknownError(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CustomerDetailsEvent.unknownError'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CustomerDetailsEventUnknownError &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CustomerDetailsEventUnknownErrorCopyWith<
          _$CustomerDetailsEventUnknownError>
      get copyWith => __$$CustomerDetailsEventUnknownErrorCopyWithImpl<
          _$CustomerDetailsEventUnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() togglePhoneAction,
    required TResult Function(String? message) unknownError,
  }) {
    return unknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? togglePhoneAction,
    TResult? Function(String? message)? unknownError,
  }) {
    return unknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? togglePhoneAction,
    TResult Function(String? message)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CustomerDetailsEventTogglePhoneAction value)
        togglePhoneAction,
    required TResult Function(CustomerDetailsEventUnknownError value)
        unknownError,
  }) {
    return unknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult? Function(CustomerDetailsEventUnknownError value)? unknownError,
  }) {
    return unknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CustomerDetailsEventTogglePhoneAction value)?
        togglePhoneAction,
    TResult Function(CustomerDetailsEventUnknownError value)? unknownError,
    required TResult orElse(),
  }) {
    if (unknownError != null) {
      return unknownError(this);
    }
    return orElse();
  }
}

abstract class CustomerDetailsEventUnknownError
    implements CustomerDetailsEvent {
  const factory CustomerDetailsEventUnknownError([final String? message]) =
      _$CustomerDetailsEventUnknownError;

  String? get message;
  @JsonKey(ignore: true)
  _$$CustomerDetailsEventUnknownErrorCopyWith<
          _$CustomerDetailsEventUnknownError>
      get copyWith => throw _privateConstructorUsedError;
}
