import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/config/routes.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/widgets/customer_details_main_infos.dart';
import 'package:scaffold_riverpod/modules/customer/details/widgets/customer_phone_action_modal.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/texts/section_title.dart';

import 'customer_details_logic.dart';
import 'widgets/customer_details_app_bar.dart';

part 'customer_details_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CustomerDetailsState with _$CustomerDetailsState {
  const factory CustomerDetailsState({
    Customer? customer,
    @Default(false) bool error,
  }) = _CustomerDetailsState;

  const CustomerDetailsState._();
}

class CustomerDetailsView extends ConsumerWidget {
  const CustomerDetailsView(this.customer, {super.key});

  final Customer customer;

  void onEvent(CustomerDetailsEvent e, BuildContext context) {
    if (e is CustomerDetailsEventTogglePhoneAction) {
      context.showModal(CustomerPhoneActionModal(customer));
    } else if (e is CustomerDetailsEventUnknownError && e.message.isNotNullOrBlank) {
      debugPrint(e.message);
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerDetailsLogic.provider(customer.id).notifier);
    ref.listen<AsyncValue<CustomerDetailsEvent>>(
        CustomerDetailsEvent.provider, (previous, next) => onEvent(next.value!, context));
    return Scaffold(
      appBar: CustomerDetailsAppBar(customer),
      floatingActionButton: Consumer(builder: (context, ref, child) {
        final stateCustomer =
            ref.watch(CustomerDetailsLogic.provider(customer.id).select((s) => s.customer)) ?? customer;
        return FloatingActionButton(
          onPressed: () => context.go(customerEditRoute(customer.id), extra: {'customer': stateCustomer}),
          child: const Icon(Icons.edit_rounded, size: $Icon.s),
        );
      }),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all($Padding.m),
            child: Consumer(
              builder: (context, ref, child) {
                final error = ref.watch(CustomerDetailsLogic.provider(customer.id).select((s) => s.error));
                return error
                    ? Center(child: Text(context.translation.unknownError))
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionTitle(text: context.translation.mainInfo, textAsHeroTag: true),
                          $Gap.s,
                          Consumer(builder: (context, ref, child) {
                            final stateCustomer =
                                ref.watch(CustomerDetailsLogic.provider(customer.id).select((s) => s.customer)) ??
                                    customer;
                            return CustomerDetailsMainInfos(logic, stateCustomer);
                          }),
                          $Gap.m,
                        ],
                      );
              },
            ),
          ),
          $Gap.xxl,
        ],
      ),
    );
  }
}
