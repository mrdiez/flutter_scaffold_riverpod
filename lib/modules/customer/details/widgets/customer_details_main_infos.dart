import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/customer_details_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/utils/string.dart';
import 'package:scaffold_riverpod/widgets/layout/rounded_material.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';

class CustomerDetailsMainInfos extends StatelessWidget {
  const CustomerDetailsMainInfos(this.logic, this.customer, {super.key});

  final CustomerDetailsLogic logic;
  final Customer? customer;

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      heroTag: 'customer_main_info_container_${customer?.id}',
      innerPadding: const EdgeInsets.all($Padding.s),
      child: SingleChildScrollView(
        child: Column(
          children: [
            InkWell(
              onTap: (customer!.phone?.isPhone ?? false) ? () => logic.onPhoneAction(customer!.phone!) : null,
              child: Row(
                children: [
                  Icon(Icons.phone_rounded, color: context.primaryColor, size: $Icon.s),
                  $Gap.s,
                  Expanded(child: _CustomerInfoRow(customer!.phone)),
                ],
              ),
            ),
            $Gap.xs,
            InkWell(
              onTap: (customer!.email?.isEmail ?? false) ? () => logic.mail(customer!.email!) : null,
              child: Row(
                children: [
                  Icon(Icons.email_rounded, color: context.primaryColor, size: $Icon.s),
                  $Gap.s,
                  Expanded(child: _CustomerInfoRow(customer!.email)),
                ],
              ),
            ),
            $Gap.xs,
            InkWell(
              onTap: customer!.fullAddress.isNotNullOrBlank ? () => logic.itinerary(customer!.fullAddress) : null,
              child: Row(
                children: [
                  Icon(Icons.place_rounded, color: context.primaryColor, size: $Icon.s),
                  $Gap.s,
                  if (customer!.fullAddress.isNotNullOrBlank)
                    Expanded(
                      child: Column(
                        children: [
                          if (customer!.address1.isNotNullOrBlank)
                            Row(children: [Expanded(child: _CustomerInfoRow(customer!.address1))]),
                          if (customer!.address2.isNotNullOrBlank)
                            Row(children: [Expanded(child: _CustomerInfoRow(customer!.address2))]),
                          if ('${customer!.postalCode} ${customer!.city}'.isNotNullOrBlank)
                            Row(
                              children: [
                                if (customer!.postalCode.isNotNullOrBlank) _CustomerInfoRow(customer!.postalCode),
                                if (customer!.postalCode.isNotNullOrBlank && customer!.city.isNotNullOrBlank) $Gap.s,
                                if (customer!.city.isNotNullOrBlank) _CustomerInfoRow(customer!.city),
                              ],
                            ),
                        ],
                      ),
                    )
                  else
                    const Text('-'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _CustomerInfoRow extends StatelessWidget {
  const _CustomerInfoRow(this.text);

  final String? text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: $Padding.xxxs, bottom: $Padding.xxs),
      child: Text(text.isNotNullOrBlank ? text! : '-'),
    );
  }
}
