import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/customer_details_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/buttons/back_icon_button.dart';

class CustomerDetailsAppBar extends AppBar {
  CustomerDetailsAppBar(this.customer, {super.key});

  final Customer customer;

  @override
  State<CustomerDetailsAppBar> createState() => _CustomerDetailsAppBarState();
}

class _CustomerDetailsAppBarState extends State<CustomerDetailsAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: const Padding(
        padding: EdgeInsets.only(top: $Padding.xxs),
        child: BackIconButton(),
      ),
      title: Consumer(
        builder: (context, ref, child) {
          final stateCustomer =
              ref.watch(CustomerDetailsLogic.provider(widget.customer.id).select((s) => s.customer)) ?? widget.customer;
          return Row(
            children: [
              Hero(
                tag: 'customer_first_name_${widget.customer.id}',
                child: AutoSizeText(
                  '${stateCustomer.firstName} ',
                  style: context.textTheme.headline2,
                  maxLines: 1,
                  // overflow: TextOverflow.clip,
                ),
              ),
              Expanded(
                child: Hero(
                  tag: 'customer_last_name_${widget.customer.id}',
                  child: AutoSizeText(
                    stateCustomer.lastName,
                    style: context.textTheme.headline2,
                    maxLines: 1,
                  ),
                ),
              ),
            ],
          );
        },
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: $Padding.m),
          child: Hero(
            tag: 'customer_avatar_${widget.customer.id}',
            child: Consumer(builder: (context, ref, child) {
              final stateCustomer =
                  ref.watch(CustomerDetailsLogic.provider(widget.customer.id).select((s) => s.customer)) ??
                      widget.customer;
              return CircleAvatar(radius: $Icon.xs, child: Text(stateCustomer.initials));
            }),
          ),
        ),
      ],
    );
  }
}
