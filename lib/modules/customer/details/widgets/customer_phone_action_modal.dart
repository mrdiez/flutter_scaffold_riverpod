import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/customer_details_logic.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/widgets/layout/rounded_material.dart';

class CustomerPhoneActionModal extends ConsumerWidget {
  const CustomerPhoneActionModal(
    this.customer, {
    super.key,
  });

  final Customer customer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(CustomerDetailsLogic.provider(customer.id).notifier);
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: $Padding.m),
        child: RoundedContainer(
          color: context.theme.snackBarTheme.backgroundColor?.withOpacity(0.95),
          child: SingleChildScrollView(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: $Padding.s),
                  child: Column(
                    children: [
                      RoundedContainer(
                        onTap: () {
                          logic.call();
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.call_rounded,
                          size: $Icon.m,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: $Padding.xs),
                        child: Text(context.translation.call),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: $Padding.s),
                  child: Column(
                    children: [
                      RoundedContainer(
                        onTap: () {
                          logic.sms();
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.sms_rounded,
                          size: $Icon.m,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: $Padding.xs),
                        child: Text(context.translation.sms),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
