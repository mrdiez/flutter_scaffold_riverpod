import 'package:freezed_annotation/freezed_annotation.dart';

// Put here some useful enums (not like this one :/)
enum TestEnum {
  @JsonValue(1)
  people,
  @JsonValue(2)
  travel,
  @JsonValue(3)
  cooking,
}
