import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/modules/customer/details/customer_details_view.dart';
import 'package:scaffold_riverpod/modules/customer/edit/customer_edit_view.dart';
import 'package:scaffold_riverpod/modules/customer/list/customer_list_view.dart';
import 'package:scaffold_riverpod/modules/home/home_view.dart';
import 'package:scaffold_riverpod/services/common_event.dart';

// App routes
const notFoundRoute = '/404';
const homeRoute = '/home';
const customerListRoute = '/customers';
const _customerDetailsRoute = 'details';
const _customerEditRoute = 'edit';
String customerDetailsRoute(int id) => '$customerListRoute/$_customerDetailsRoute/$id';
const customerCreateRoute = '$customerListRoute/$_customerEditRoute';
String customerEditRoute(int id) => '${customerDetailsRoute(id)}/$_customerEditRoute';

// The error 404 page content
Widget getNotFoundScreen(GoRouterState state) =>
    Material(child: Center(child: Text('404: ${state.fullpath} not found')));
MaterialPage getNotFoundPage(GoRouterState state) => MaterialPage(child: getNotFoundScreen(state));

final routes = GoRouter(
  observers: [HeroController()],
  routes: [
    GoRoute(
      path: '/',
      redirect: (context, state) => homeRoute,
    ),
    GoRoute(
      path: homeRoute,
      builder: (context, state) => const HomeView(),
    ),
    GoRoute(
      path: customerListRoute,
      builder: (context, state) => const CustomerListView(),
      routes: [
        GoRoute(
            path: '$_customerDetailsRoute/:id',
            builder: (context, state) {
              final id = int.tryParse(state.params['id']!);
              final extraParams = state.extra as Map<String, dynamic>;
              final customer = extraParams['customer'] as Customer;
              if (id == null || id != customer.id) return getNotFoundScreen(state);
              return CustomerDetailsView(customer);
            },
            routes: [
              GoRoute(
                path: _customerEditRoute,
                builder: (context, state) {
                  final id = int.tryParse(state.params['id']!);
                  final extraParams = state.extra as Map<String, dynamic>;
                  final customer = extraParams['customer'] as Customer;
                  if (id == null || id != customer.id) return getNotFoundScreen(state);
                  return CustomerEditView(customer);
                },
              ),
            ]),
        GoRoute(
          path: _customerEditRoute,
          builder: (context, state) => const CustomerEditView(null),
        ),
      ],
    ),
  ],
  errorPageBuilder: (context, state) => getNotFoundPage(state),
);

// A little guardian that allow to navigate to a specific route on pop or to not navigate at all
Future<bool> onWillPop() async {
  CommonEvent.streamController?.add(const CommonEvent.pop());
  final fallbackRoute = _getRouteOnPop();
  if (fallbackRoute != null && routes.routerDelegate.navigatorKey.currentContext != null) {
    routes.routerDelegate.navigatorKey.currentContext!.go(fallbackRoute);
  } else if (_doesRouteCanPop()) {
    if (routes.routerDelegate.canPop()) {
      routes.routerDelegate.pop();
    } else {
      return true;
    }
  }
  return false;
}

typedef _RoutePopGetter = String? Function(String route);
typedef _RoutePopTester = bool Function(String route);

String? _getRouteOnPop() {
  for (final test in _routePopGetters) {
    if (routes.routeInformationProvider.value.location != null) {
      final fallbackRoute = test(routes.routeInformationProvider.value.location!);
      if (fallbackRoute != null) return fallbackRoute;
    }
  }
  return null;
}

final _routePopGetters = <_RoutePopGetter>[
  (String route) => route == customerListRoute ? homeRoute : null,
];

bool _doesRouteCanPop() => !_routeShouldNotPopTesters.any((test) =>
    routes.routeInformationProvider.value.location != null && test(routes.routeInformationProvider.value.location!));

final _routeShouldNotPopTesters = <_RoutePopTester>[
  (String route) => route.startsWith(customerListRoute) && route.endsWith(_customerEditRoute),
];
