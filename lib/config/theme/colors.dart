import 'package:flutter/material.dart';
import 'package:ms_material_color/ms_material_color.dart';
import 'package:scaffold_riverpod/utils/color.dart';

final primaryColor = blueColor;
final secondaryColor = yellowColor;

// Main colors palette
final blueColor = MsMaterialColor(0xff8888EB);
final yellowColor = MsMaterialColor(0xffFCA311);
final redColor = MsMaterialColor(0xffBF4D69);
final greenColor = MsMaterialColor(0xffBCCA6B);
final pinkColor = MsMaterialColor(0xffC289B7);

// Clickable, selectable elements
final selectedColor = primaryColor;
final activeColor = darkGreyColor;
final disabledColor = greyColor;
final linkColor = primaryColor;

// Feedback colors
final positiveColor = greenColor;
final negativeColor = redColor;
final warningColor = yellowColor;
final informativeColor = blueColor;

// Main layout elements
const headerColor = whiteColor;
final mainBackgroundColor = MsMaterialColor(primaryLighterColor.value);
final panelBackgroundColor = primaryLightestColor;
const navBackgroundColor = whiteColor;

// Shadow colors
final lightShadowColor = whiteColor.withOpacity(0.75);
final darkShadowColor = greyColor.withOpacity(0.25);

// 50 shades of primaryColor
final primaryDarkestColor = primaryColor.darken(88);
final primaryDarkerColor = primaryColor.darken(77);
final primaryDarkColor = primaryColor.darken(50);
final primaryLightColor = primaryColor.lighten(50);
final primaryLighterColor = primaryColor.lighten(77);
final primaryLightestColor = primaryColor.lighten(88);

// 50 shades of grey
const blackColor = Colors.black;
final darkColor = MsMaterialColor(0xff414141);
final darkGreyColor = MsMaterialColor(0xff747474);
final greyColor = MsMaterialColor(0xffA7A7A7);
final lightGreyColor = MsMaterialColor(0xffD9D9D9);
final lightColor = MsMaterialColor(0xffE6E6E6);
const whiteColor = Colors.white;
const transparentColor = Colors.transparent;
