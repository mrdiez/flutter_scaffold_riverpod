import 'package:flutter/material.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';

import 'colors.dart';

ThemeData defaultTheme = getTheme(primaryColor, secondaryColor, mainBackgroundColor);

ThemeData getTheme(MaterialColor primaryColor, MaterialColor secondaryColor, MaterialColor backgroundColor) =>
    ThemeData(
      brightness: Brightness.light,
      backgroundColor: backgroundColor,
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: primaryColor,
        selectedItemColor: whiteColor,
        selectedIconTheme: const IconThemeData(color: blackColor),
        unselectedIconTheme: const IconThemeData(color: whiteColor),
        unselectedLabelStyle: const TextStyle(color: whiteColor),
      ),
      canvasColor: whiteColor,
      chipTheme: ChipThemeData(
        backgroundColor: primaryColor,
        deleteIconColor: whiteColor,
      ),
      colorScheme: ColorScheme.fromSwatch(
        accentColor: secondaryColor,
        backgroundColor: backgroundColor,
        brightness: Brightness.light,
        primarySwatch: primaryColor,
        errorColor: negativeColor,
        cardColor: whiteColor,
        primaryColorDark: primaryDarkColor,
      ),
      disabledColor: disabledColor,
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
        foregroundColor: MaterialStateProperty.resolveWith<Color?>(
          (Set<MaterialState> states) {
            return whiteColor; // Use the component's default.
          },
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color?>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed)) {
              return secondaryColor.shade500;
            }
            if (states.contains(MaterialState.disabled)) {
              return disabledColor;
            }
            return secondaryColor; // Use the component's default.
          },
        ),
      )),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: secondaryColor,
      ),
      iconTheme: IconThemeData(color: primaryColor),
      highlightColor: primaryColor.withOpacity(0.25),
      navigationBarTheme: NavigationBarThemeData(indicatorColor: darkGreyColor),
      primarySwatch: primaryColor,
      snackBarTheme: SnackBarThemeData(backgroundColor: panelBackgroundColor),
      scaffoldBackgroundColor: backgroundColor,
      splashColor: primaryColor.withOpacity(0.1),
      textTheme: TextTheme(
        bodyText1: TextStyle(
          fontSize: $Font.m,
          color: darkGreyColor,
          height: 1,
        ),
        bodyText2: TextStyle(
          fontSize: $Font.s,
          color: darkGreyColor,
          height: 1,
        ),
        button: TextStyle(
          fontSize: $Font.m,
          fontWeight: FontWeight.w600,
          color: darkGreyColor,
          height: 1,
        ),
        caption: TextStyle(
          fontSize: $Font.s,
          color: greyColor,
          height: 1,
        ),
        headline1: TextStyle(
          fontSize: $Font.xxxl,
          fontWeight: FontWeight.w900,
          color: primaryDarkerColor,
          height: 1,
        ),
        headline2: TextStyle(
          fontSize: $Font.xxl,
          fontWeight: FontWeight.w900,
          color: primaryDarkerColor,
          height: 1,
        ),
        headline3: TextStyle(
          fontSize: $Font.xxl,
          fontWeight: FontWeight.w600,
          color: primaryDarkerColor,
          height: 1,
        ),
        headline4: TextStyle(
          fontSize: $Font.xl,
          fontWeight: FontWeight.w600,
          color: primaryDarkerColor,
          height: 1,
        ),
        headline5: TextStyle(
          fontSize: $Font.l,
          fontWeight: FontWeight.w900,
          color: greyColor,
          height: 1,
        ),
        headline6: TextStyle(
          fontSize: $Font.l,
          fontWeight: FontWeight.w600,
          color: primaryDarkerColor,
          height: 1,
        ),
        subtitle1: TextStyle(
          fontSize: $Font.l,
          fontWeight: FontWeight.w600,
          color: primaryDarkerColor,
          height: 1,
        ),
        subtitle2: TextStyle(
          fontSize: $Font.s,
          color: greyColor,
          height: 1,
        ),
      ),
      useMaterial3: true,
    );
