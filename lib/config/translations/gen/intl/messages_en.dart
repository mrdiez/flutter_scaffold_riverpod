// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(nb) => "${nb} characters minimum";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_DETAILS________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_EDIT__________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_LIST___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("en"),
        "address": MessageLookupByLibrary.simpleMessage("Address"),
        "autoDisposeProviderExample": MessageLookupByLibrary.simpleMessage(
            "Auto dispose provider example"),
        "call": MessageLookupByLibrary.simpleMessage("Call"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "city": MessageLookupByLibrary.simpleMessage("City"),
        "completeExample":
            MessageLookupByLibrary.simpleMessage("Complete example"),
        "customers": MessageLookupByLibrary.simpleMessage("Customers"),
        "edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "familyProviderExample":
            MessageLookupByLibrary.simpleMessage("Family provider example"),
        "firstName": MessageLookupByLibrary.simpleMessage("First name"),
        "firstNameRequired":
            MessageLookupByLibrary.simpleMessage("First name is required"),
        "goTo": MessageLookupByLibrary.simpleMessage("Go to"),
        "hello": MessageLookupByLibrary.simpleMessage("Hello"),
        "increment": MessageLookupByLibrary.simpleMessage("Increment:"),
        "lastName": MessageLookupByLibrary.simpleMessage("Last name"),
        "lastNameRequired":
            MessageLookupByLibrary.simpleMessage("Last name is required"),
        "leave": MessageLookupByLibrary.simpleMessage("Leave"),
        "locale": MessageLookupByLibrary.simpleMessage("en"),
        "mainInfo": MessageLookupByLibrary.simpleMessage("Main information"),
        "nbCharactersMinimum": m0,
        "nestedProviderExample":
            MessageLookupByLibrary.simpleMessage("Nested provider example"),
        "networkError": MessageLookupByLibrary.simpleMessage(
            "Network error, please check your connectivity"),
        "notAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Some characters are not allowed"),
        "notValidEmail":
            MessageLookupByLibrary.simpleMessage("This is not a valid email"),
        "notValidPhone": MessageLookupByLibrary.simpleMessage(
            "This is not a valid phone number"),
        "notValidPostalCode": MessageLookupByLibrary.simpleMessage(
            "This is not a valid postal code"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "phone": MessageLookupByLibrary.simpleMessage("Phone"),
        "postalCode": MessageLookupByLibrary.simpleMessage("Postal code"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "searchCustomer":
            MessageLookupByLibrary.simpleMessage("Search customer"),
        "simpleProviderExample":
            MessageLookupByLibrary.simpleMessage("Simple provider example"),
        "sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "submit": MessageLookupByLibrary.simpleMessage("Submit"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("This field is required"),
        "unknownError":
            MessageLookupByLibrary.simpleMessage("Unknown error, no chance."),
        "youReachedMaxValue": MessageLookupByLibrary.simpleMessage(
            "You reached max counter value"),
        "youWillLooseUnsavedModifications":
            MessageLookupByLibrary.simpleMessage(
                "You will loose all unsaved modifications")
      };
}
