// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(nb) => "${nb} caractères minimum";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_DETAILS________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_EDIT__________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____CUSTOMER_LIST___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("fr"),
        "address": MessageLookupByLibrary.simpleMessage("Addresse"),
        "autoDisposeProviderExample": MessageLookupByLibrary.simpleMessage(
            "Exemple: Auto dispose provider"),
        "call": MessageLookupByLibrary.simpleMessage("Appel"),
        "cancel": MessageLookupByLibrary.simpleMessage("Annuler"),
        "city": MessageLookupByLibrary.simpleMessage("Ville"),
        "completeExample":
            MessageLookupByLibrary.simpleMessage("Exemple complet"),
        "customers": MessageLookupByLibrary.simpleMessage("Clients"),
        "edit": MessageLookupByLibrary.simpleMessage("Modifier"),
        "email": MessageLookupByLibrary.simpleMessage("E-mail"),
        "familyProviderExample":
            MessageLookupByLibrary.simpleMessage("Exemple: family provider"),
        "firstName": MessageLookupByLibrary.simpleMessage("Prénom"),
        "firstNameRequired":
            MessageLookupByLibrary.simpleMessage("Le prénom est requis"),
        "goTo": MessageLookupByLibrary.simpleMessage("Aller vers"),
        "hello": MessageLookupByLibrary.simpleMessage("Bonjour"),
        "increment": MessageLookupByLibrary.simpleMessage("Incrémenter:"),
        "lastName": MessageLookupByLibrary.simpleMessage("Nom"),
        "lastNameRequired": MessageLookupByLibrary.simpleMessage(
            "Le nom de famille est requis"),
        "leave": MessageLookupByLibrary.simpleMessage("Quitter"),
        "locale": MessageLookupByLibrary.simpleMessage("fr"),
        "mainInfo":
            MessageLookupByLibrary.simpleMessage("Informations principales"),
        "nbCharactersMinimum": m0,
        "nestedProviderExample":
            MessageLookupByLibrary.simpleMessage("Exemple: Nested provider"),
        "networkError": MessageLookupByLibrary.simpleMessage(
            "Erreur réseau, merci de vérifier votre connexion."),
        "notAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Certains caractères ne sont pas autorisés"),
        "notValidEmail": MessageLookupByLibrary.simpleMessage("Email invalide"),
        "notValidPhone": MessageLookupByLibrary.simpleMessage(
            "Numéro de téléphone invalide"),
        "notValidPostalCode":
            MessageLookupByLibrary.simpleMessage("Code postal invalide"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "phone": MessageLookupByLibrary.simpleMessage("Téléphone"),
        "postalCode": MessageLookupByLibrary.simpleMessage("Code postal"),
        "save": MessageLookupByLibrary.simpleMessage("Enregistrer"),
        "searchCustomer":
            MessageLookupByLibrary.simpleMessage("Rechercher un client"),
        "simpleProviderExample":
            MessageLookupByLibrary.simpleMessage("Exemple: simple provider"),
        "sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "submit": MessageLookupByLibrary.simpleMessage("Envoyer"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("Ce champ est obligatoire"),
        "unknownError": MessageLookupByLibrary.simpleMessage(
            "Erreur inconnue, pas de chance."),
        "youReachedMaxValue": MessageLookupByLibrary.simpleMessage(
            "Vous avez atteint la valeur maximale"),
        "youWillLooseUnsavedModifications":
            MessageLookupByLibrary.simpleMessage(
                "Vous allez perdre vos modifications")
      };
}
