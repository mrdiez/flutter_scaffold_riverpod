// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class Translation {
  Translation();

  static Translation? _current;

  static Translation get current {
    assert(_current != null,
        'No instance of Translation was loaded. Try to initialize the Translation delegate before accessing Translation.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<Translation> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = Translation();
      Translation._current = instance;

      return instance;
    });
  }

  static Translation of(BuildContext context) {
    final instance = Translation.maybeOf(context);
    assert(instance != null,
        'No instance of Translation present in the widget tree. Did you add Translation.delegate in localizationsDelegates?');
    return instance!;
  }

  static Translation? maybeOf(BuildContext context) {
    return Localizations.of<Translation>(context, Translation);
  }

  /// `fr`
  String get _locale {
    return Intl.message(
      'fr',
      name: '_locale',
      desc: '',
      args: [],
    );
  }

  /// `fr`
  String get locale {
    return Intl.message(
      'fr',
      name: 'locale',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON__________________________________ {
    return Intl.message(
      '',
      name: '_____COMMON__________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Bonjour`
  String get hello {
    return Intl.message(
      'Bonjour',
      name: 'hello',
      desc: '',
      args: [],
    );
  }

  /// `Annuler`
  String get cancel {
    return Intl.message(
      'Annuler',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Envoyer`
  String get submit {
    return Intl.message(
      'Envoyer',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Quitter`
  String get leave {
    return Intl.message(
      'Quitter',
      name: 'leave',
      desc: '',
      args: [],
    );
  }

  /// `Enregistrer`
  String get save {
    return Intl.message(
      'Enregistrer',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Modifier`
  String get edit {
    return Intl.message(
      'Modifier',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Appel`
  String get call {
    return Intl.message(
      'Appel',
      name: 'call',
      desc: '',
      args: [],
    );
  }

  /// `SMS`
  String get sms {
    return Intl.message(
      'SMS',
      name: 'sms',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Vous allez perdre vos modifications`
  String get youWillLooseUnsavedModifications {
    return Intl.message(
      'Vous allez perdre vos modifications',
      name: 'youWillLooseUnsavedModifications',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON_ERRORS___________________________ {
    return Intl.message(
      '',
      name: '_____COMMON_ERRORS___________________________',
      desc: '',
      args: [],
    );
  }

  /// `{nb} caractères minimum`
  String nbCharactersMinimum(Object nb) {
    return Intl.message(
      '$nb caractères minimum',
      name: 'nbCharactersMinimum',
      desc: '',
      args: [nb],
    );
  }

  /// `Ce champ est obligatoire`
  String get thisFieldIsRequired {
    return Intl.message(
      'Ce champ est obligatoire',
      name: 'thisFieldIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `Certains caractères ne sont pas autorisés`
  String get notAllowedCharacters {
    return Intl.message(
      'Certains caractères ne sont pas autorisés',
      name: 'notAllowedCharacters',
      desc: '',
      args: [],
    );
  }

  /// `Erreur réseau, merci de vérifier votre connexion.`
  String get networkError {
    return Intl.message(
      'Erreur réseau, merci de vérifier votre connexion.',
      name: 'networkError',
      desc: '',
      args: [],
    );
  }

  /// `Erreur inconnue, pas de chance.`
  String get unknownError {
    return Intl.message(
      'Erreur inconnue, pas de chance.',
      name: 'unknownError',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____HOME____________________________________ {
    return Intl.message(
      '',
      name: '_____HOME____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Vous avez atteint la valeur maximale`
  String get youReachedMaxValue {
    return Intl.message(
      'Vous avez atteint la valeur maximale',
      name: 'youReachedMaxValue',
      desc: '',
      args: [],
    );
  }

  /// `Exemple: simple provider`
  String get simpleProviderExample {
    return Intl.message(
      'Exemple: simple provider',
      name: 'simpleProviderExample',
      desc: '',
      args: [],
    );
  }

  /// `Exemple: family provider`
  String get familyProviderExample {
    return Intl.message(
      'Exemple: family provider',
      name: 'familyProviderExample',
      desc: '',
      args: [],
    );
  }

  /// `Exemple: Auto dispose provider`
  String get autoDisposeProviderExample {
    return Intl.message(
      'Exemple: Auto dispose provider',
      name: 'autoDisposeProviderExample',
      desc: '',
      args: [],
    );
  }

  /// `Exemple: Nested provider`
  String get nestedProviderExample {
    return Intl.message(
      'Exemple: Nested provider',
      name: 'nestedProviderExample',
      desc: '',
      args: [],
    );
  }

  /// `Incrémenter:`
  String get increment {
    return Intl.message(
      'Incrémenter:',
      name: 'increment',
      desc: '',
      args: [],
    );
  }

  /// `Exemple complet`
  String get completeExample {
    return Intl.message(
      'Exemple complet',
      name: 'completeExample',
      desc: '',
      args: [],
    );
  }

  /// `Aller vers`
  String get goTo {
    return Intl.message(
      'Aller vers',
      name: 'goTo',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____CUSTOMER_LIST___________________________ {
    return Intl.message(
      '',
      name: '_____CUSTOMER_LIST___________________________',
      desc: '',
      args: [],
    );
  }

  /// `Clients`
  String get customers {
    return Intl.message(
      'Clients',
      name: 'customers',
      desc: '',
      args: [],
    );
  }

  /// `Rechercher un client`
  String get searchCustomer {
    return Intl.message(
      'Rechercher un client',
      name: 'searchCustomer',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____CUSTOMER_DETAILS________________________ {
    return Intl.message(
      '',
      name: '_____CUSTOMER_DETAILS________________________',
      desc: '',
      args: [],
    );
  }

  /// `Informations principales`
  String get mainInfo {
    return Intl.message(
      'Informations principales',
      name: 'mainInfo',
      desc: '',
      args: [],
    );
  }

  /// `Prénom`
  String get firstName {
    return Intl.message(
      'Prénom',
      name: 'firstName',
      desc: '',
      args: [],
    );
  }

  /// `Nom`
  String get lastName {
    return Intl.message(
      'Nom',
      name: 'lastName',
      desc: '',
      args: [],
    );
  }

  /// `Téléphone`
  String get phone {
    return Intl.message(
      'Téléphone',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `E-mail`
  String get email {
    return Intl.message(
      'E-mail',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Addresse`
  String get address {
    return Intl.message(
      'Addresse',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Code postal`
  String get postalCode {
    return Intl.message(
      'Code postal',
      name: 'postalCode',
      desc: '',
      args: [],
    );
  }

  /// `Ville`
  String get city {
    return Intl.message(
      'Ville',
      name: 'city',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____CUSTOMER_EDIT__________________________ {
    return Intl.message(
      '',
      name: '_____CUSTOMER_EDIT__________________________',
      desc: '',
      args: [],
    );
  }

  /// `Le prénom est requis`
  String get firstNameRequired {
    return Intl.message(
      'Le prénom est requis',
      name: 'firstNameRequired',
      desc: '',
      args: [],
    );
  }

  /// `Le nom de famille est requis`
  String get lastNameRequired {
    return Intl.message(
      'Le nom de famille est requis',
      name: 'lastNameRequired',
      desc: '',
      args: [],
    );
  }

  /// `Numéro de téléphone invalide`
  String get notValidPhone {
    return Intl.message(
      'Numéro de téléphone invalide',
      name: 'notValidPhone',
      desc: '',
      args: [],
    );
  }

  /// `Email invalide`
  String get notValidEmail {
    return Intl.message(
      'Email invalide',
      name: 'notValidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Code postal invalide`
  String get notValidPostalCode {
    return Intl.message(
      'Code postal invalide',
      name: 'notValidPostalCode',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Translation> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Translation> load(Locale locale) => Translation.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
