import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'common_event.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class CommonEvent with _$CommonEvent {
  static StreamController<CommonEvent>? streamController;

  static final provider = StreamProvider<CommonEvent>((ref) {
    streamController = StreamController<CommonEvent>();
    ref.onDispose(streamController!.close);
    return streamController!.stream;
  });

  const factory CommonEvent.pop() = CommonEventPop;
}
