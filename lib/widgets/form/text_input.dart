import 'package:scaffold_riverpod/config/theme/colors.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TextInput extends ConsumerWidget {
  const TextInput({
    Key? key,
    this.textEditingController,
    this.hintText,
    this.onChanged,
    this.onSubmitted,
    this.style,
    this.error,
    this.contentPadding,
    this.height,
    this.color,
  }) : super(key: key);

  final TextEditingController? textEditingController;
  final String? hintText;
  final void Function(String?)? onChanged;
  final void Function(String?)? onSubmitted;
  final TextStyle? style;
  final String? error;
  final EdgeInsets? contentPadding;
  final double? height;
  final Color? color;

  bool get isError => error.isNotNullOrBlank;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final textStyle = style ?? context.textTheme.bodyText1;
    final inputHeight = isError ? height ?? $InputHeight.m : height ?? $InputHeight.s;
    return SizedBox(
      height: inputHeight,
      child: Material(
        borderRadius: const BorderRadius.all($Radius.m),
        color: color ?? context.theme.inputDecorationTheme.fillColor,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: TextField(
                controller: textEditingController,
                style: textStyle?.copyWith(overflow: TextOverflow.visible),
                decoration: InputDecoration(
                  contentPadding: contentPadding ??
                      const EdgeInsets.only(
                        left: $Padding.s,
                        right: $Padding.s,
                        bottom: $Padding.s,
                        top: $Padding.xxs,
                      ),
                  constraints: BoxConstraints.tightFor(height: inputHeight),
                  hintText: hintText,
                  hintStyle: context.textTheme.bodyText2,
                  border: InputBorder.none,
                ),
                onChanged: onChanged,
                onSubmitted: onSubmitted,
              ),
            ),
            if (isError || (textEditingController?.text).isNotNullOrBlank)
              Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  icon: const Icon(
                    Icons.close_rounded,
                    size: $Icon.s,
                  ),
                  color: negativeColor,
                  // onTap: logic.resetUrl,
                  onPressed: null,
                ),
              ),
            if (isError)
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: $Padding.s, right: $Padding.s, bottom: $Padding.xxxs),
                  child: Text(error!, style: context.theme.inputDecorationTheme.errorStyle),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
