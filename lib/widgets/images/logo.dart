import 'package:easter_egg_trigger/easter_egg_trigger.dart';
import 'package:flutter/material.dart';
import 'package:scaffold_riverpod/config/assets.gen.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:url_launcher/url_launcher.dart';

class Logo extends StatelessWidget {
  const Logo({Key? key, this.onTap, this.height, this.axis = Axis.horizontal}) : super(key: key);

  final void Function()? onTap;
  final double? height;
  final Axis axis;

  @override
  Widget build(BuildContext context) {
    final imageHeight = height ?? $LogoHeight.m;
    final logo = Hero(
      tag: 'LOGO',
      child: axis == Axis.vertical
          ? Assets.logos.logoVerticalColor.image(height: imageHeight)
          : Assets.logos.logoHorizontalColor.image(height: imageHeight),
    );
    return SizedBox(
        height: imageHeight,
        child: onTap != null
            ? InkWell(onTap: onTap!, child: logo)
            : EasterEggTrigger(
                action: () async {
                  final ohYeah = Uri(path: 'https://www.youtube.com/watch?v=GMy6Is5JPQU');
                  try {
                    await launchUrl(ohYeah);
                  } catch (e) {
                    debugPrint('Unable to launch url: ${ohYeah.path}');
                  }
                },
                child: logo,
              ));
  }
}
