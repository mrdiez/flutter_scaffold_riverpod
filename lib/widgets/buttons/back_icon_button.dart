import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BackIconButton extends StatelessWidget {
  const BackIconButton({super.key, this.onTap});

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'back_icon_button',
      child: IconButton(
        icon: const Padding(
          padding: EdgeInsets.only(bottom: $Padding.xs),
          child: Icon(Icons.arrow_back_ios_new_rounded),
        ),
        iconSize: $Icon.s,
        onPressed: () {
          if (onTap != null) {
            onTap!();
          } else {
            context.pop();
          }
        },
      ),
    );
  }
}
