import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/utils/widget.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    super.key,
    this.title,
    this.text,
    this.dividerColor,
    this.textAsHeroTag = false,
  }) : assert((title != null && text == null) || (title == null && text != null));

  final Widget? title;
  final String? text;
  final bool textAsHeroTag;
  final Color? dividerColor;

  double getTitleHeight(BuildContext context) =>
      (context.textTheme.headline4?.fontSize ?? 0) * (context.textTheme.headline4?.height ?? 1);
  double getDividerWidth(BuildContext context) => getTitleHeight(context) * 3;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return SizedBox(
        height: getTitleHeight(context) + $DividerHeight.l + $Padding.xxs,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (title.isNotNull) title!,
            if (text.isNotNullOrBlank) Text(text!, style: context.textTheme.headline4),
            Padding(
              padding: const EdgeInsets.only(top: $Padding.xxs),
              child: Container(
                height: $DividerHeight.m,
                width: getDividerWidth(context),
                decoration: BoxDecoration(
                  color: dividerColor ?? context.secondaryColor,
                  borderRadius: const BorderRadius.all($Radius.l),
                ),
              ),
            ),
          ],
        ),
      ).wrapWithHero(textAsHeroTag ? text : null);
    });
  }
}
