import 'package:scaffold_riverpod/config/theme/colors.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/utils/context.dart';
import 'package:scaffold_riverpod/utils/widget.dart';
import 'package:flutter/material.dart';

class RoundedContainer extends StatelessWidget {
  const RoundedContainer({
    Key? key,
    this.child,
    this.heroTag,
    this.onlyBackgroundHero = false,
    this.color,
    this.innerPadding,
    this.borderRadius,
    this.boxShadow,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  final Widget? child;
  final String? heroTag;
  final bool onlyBackgroundHero;
  final Color? color;
  final EdgeInsets? innerPadding;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  final VoidCallback? onTap;
  final VoidCallback? onLongPress;

  @override
  Widget build(BuildContext context) {
    return !onlyBackgroundHero
        ? Container(
            decoration: BoxDecoration(
              borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
              boxShadow: boxShadow,
            ),
            child: Material(
              color: color ?? context.theme.canvasColor,
              borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
              child: Padding(
                padding: innerPadding ?? const EdgeInsets.all($Padding.s),
                child: child ?? Container(),
              ).wrapWithInkWell(
                onTap,
                onLongPress: onLongPress,
                borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
              ),
            ),
          ).wrapWithHero(heroTag)
        : Stack(
            children: [
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
                    boxShadow: boxShadow,
                  ),
                  child: Material(
                    color: color ?? context.theme.canvasColor,
                    borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
                    child: Container(),
                  ),
                ).wrapWithHero(heroTag),
              ),
              Material(
                color: transparentColor,
                child: Padding(
                  padding: innerPadding ?? const EdgeInsets.all($Padding.s),
                  child: child ?? Container(),
                ).wrapWithInkWell(
                  onTap,
                  onLongPress: onLongPress,
                  borderRadius: borderRadius ?? const BorderRadius.all($Radius.xs),
                ),
              )
            ],
          );
  }
}
