abstract class Repository<T> {
  Stream<List<T>> watchAll();

  Stream<T?> watch(int id);

  Future<T?> get(int id);

  Future<List<T>> getAll();

  Future<int> insert(T entry);

  Future<List<int>> insertAll(List<T> entries);

  Future<int> update(T entry);

  Future<List<int>> updateAll(List<T> entries);

  Future<bool> delete(int id);

  Future<int> deleteAll(List<int> ids);
}
