import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scaffold_riverpod/config/environment.dart';
import 'package:scaffold_riverpod/config/routes.dart';
import 'package:scaffold_riverpod/config/theme/sizes.dart';
import 'package:scaffold_riverpod/config/theme/theme.dart';
import 'package:scaffold_riverpod/config/translations/gen/l10n.dart';
import 'package:scaffold_riverpod/data/access/network_api.dart';
import 'package:scaffold_riverpod/data/mocks/mocked_network_api.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/services/common_providers.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Lock rotation on small screens
  final windowSize = MediaQueryData.fromWindow(window).size;
  if ($Screen.size(min(windowSize.height, windowSize.width)) < xs) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }
  final isar = await Isar.open([CustomerSchema]);
  Environment.appDirectory = await getApplicationDocumentsDirectory();
  runApp(ProviderScope(
    overrides: [
      isarProvider.overrideWithValue(isar),
      if (Environment.isMock) NetworkApi.provider.overrideWithValue(MockedNetworkApi()),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: defaultTheme,
        localizationsDelegates: const [
          Translation.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: Translation.delegate.supportedLocales,
        home: WillPopScope(
          onWillPop: onWillPop,
          child: Material(
            child: Router(
              routeInformationProvider: routes.routeInformationProvider,
              routeInformationParser: routes.routeInformationParser,
              routerDelegate: routes.routerDelegate,
            ),
          ),
        ),
      ),
    );
  }
}
