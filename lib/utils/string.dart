import 'package:dartx/dartx.dart';
import 'package:diacritic/diacritic.dart' as diacritic;
import 'package:fzregex/fzregex.dart';
import 'package:fzregex/utils/pattern.dart';

extension StringUtils on String {
  bool get isValidName => RegExp(r"^[\p{L} ,.'-]*$", caseSensitive: false, unicode: true, dotAll: true).hasMatch(this);
  bool get isValidUrl {
    if (isEmpty || isBlank) return false;
    final uri = Uri.tryParse(this);
    return uri != null && uri.hasAbsolutePath && uri.scheme.startsWith('http');
  }

  String removeDiacritics() => diacritic.removeDiacritics(this);

  String clean() => toLowerCase().removeDiacritics();

  List<String> splitWords() => replaceAll(r'[^\p{IsAlphabetic}]', ' ').split(' ');

  List<String> cleanAndSplit() => clean().splitWords();

  bool get isEmail => Fzregex.hasMatch(this, FzPattern.email);
  bool get isPhone => Fzregex.hasMatch(this, FzPattern.phone);
  bool get isPostalCode => Fzregex.hasMatch(this, FzPattern.postalCode);

  String getAllAfterFirst(String target) {
    if (!contains(target)) return this;
    return substring(indexOf(target) + target.length);
  }

  String getAllAfterLast(String target) {
    if (!contains(target)) return this;
    return substring(lastIndexOf(target) + target.length);
  }
}
