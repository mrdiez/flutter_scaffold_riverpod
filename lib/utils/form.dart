import 'package:flutter/material.dart';

extension TextEditingControllerUtils on TextEditingController {
  void setValue(String? newValue) => value = TextEditingValue(text: newValue ?? '');
}
