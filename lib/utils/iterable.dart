extension ListUtils on List {
  void addAllNotContained(List<dynamic> entries) => addAll(entries.where((e) => !contains(e)));
}

extension IterableNullableUtils on Iterable? {
  bool get isNotNullOrEmpty => this != null && this!.isNotEmpty;
  bool get isNullOrEmpty => !isNotNullOrEmpty;
}
