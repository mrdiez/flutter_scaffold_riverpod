import 'package:flutter/material.dart';

extension WidgetUtils on Widget {
  Widget wrapWithHero(String? tag) => tag != null ? Hero(tag: tag, child: this) : this;
  Widget wrapWithInkWell(
    VoidCallback? onTap, {
    BorderRadius? borderRadius,
    VoidCallback? onLongPress,
  }) =>
      onTap != null || onLongPress != null
          ? InkWell(
              onTap: onTap,
              onLongPress: onLongPress,
              borderRadius: borderRadius,
              child: this,
            )
          : this;
}

extension WidgetNullableUtils on Widget? {
  bool get isNotNull => this != null;
}
