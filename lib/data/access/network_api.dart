import 'dart:convert';
import 'dart:io';

import 'package:scaffold_riverpod/config/environment.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NetworkApi {
  static final provider = Provider<NetworkApi>((ref) => NetworkApi());

  final Dio client = Dio(BaseOptions(baseUrl: Environment.baseUrl));
  final headers = {
    HttpHeaders.acceptHeader: 'json/application/json',
    HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
  };

  Future<Map<String, dynamic>> get(int id, String path) async {
    final target = '${Environment.baseUrl}$path/$id';
    debugPrint('$runtimeType get $id\n$target');
    try {
      final response = await client.get(target, options: Options(headers: headers));
      checkResponse(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      debugPrint('$runtimeType get response: $responseData');
      return responseData;
    } catch (e, stackTrace) {
      debugPrint('$runtimeType get $id ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<List<Map<String, dynamic>>> getMany(String path) async {
    final target = '${Environment.baseUrl}$path';
    debugPrint('$runtimeType getMany\n$target');
    try {
      final response = await client.get(target, options: Options(headers: headers));
      checkResponse(response);
      List<dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData as List<Map<String, dynamic>>;
    } catch (e, stackTrace) {
      debugPrint('$runtimeType getMany ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<Map<String, dynamic>> post(Map<String, dynamic> data, String path) async {
    final target = '${Environment.baseUrl}$path';
    debugPrint('$runtimeType post: $data\n$target');
    try {
      final response = await client.post(target, data: data, options: Options(headers: headers));
      checkResponse(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData;
    } catch (e, stackTrace) {
      debugPrint('$runtimeType post: $data ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  void checkResponse(Response response) {
    if (response.statusCode != 200) {
      throw 'Status: ${response.statusCode}\nResponse:${response.statusMessage}';
    }
  }
}
