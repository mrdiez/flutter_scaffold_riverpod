import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:isar/isar.dart';
import 'package:scaffold_riverpod/abstract/interfaces/repository.dart';
import 'package:scaffold_riverpod/data/access/network_api.dart';
import 'package:scaffold_riverpod/data/models/customer.dart';
import 'package:scaffold_riverpod/services/common_providers.dart';
import 'package:scaffold_riverpod/utils/iterable.dart';

class CustomerRepository implements Repository<Customer> {
  static final provider = Provider((ref) {
    return CustomerRepository(
      ref.watch(CustomerLocalRepository.provider),
      ref.watch(CustomerNetworkRepository.provider),
    );
  });

  CustomerRepository(this._localRepository, this._networkRepository) {
    _customerStreamController = StreamController<List<Customer>>(onCancel: () => _localRepositorySubscription.cancel());
    _localRepositorySubscription = _localRepository.watchAll().map((value) {
      debugPrint('[$runtimeType] stream event\n$value');
      return value;
    }).listen(_customerStreamController.add);
  }

  final CustomerLocalRepository _localRepository;
  final CustomerNetworkRepository _networkRepository;

  late final StreamController<List<Customer>> _customerStreamController;
  late final StreamSubscription<List<Customer>> _localRepositorySubscription;

  void _triggerStreamAllEvent() => getAll().then(_customerStreamController.add);

  @override
  Stream<List<Customer>> watchAll() {
    _triggerStreamAllEvent();
    return _customerStreamController.stream;
  }

  @override
  Stream<Customer?> watch(int id) => _localRepository.watch(id);

  @override
  Future<Customer?> get(int id) => _localRepository.get(id);

  @override
  Future<List<Customer>> getAll() async {
    final localCustomers = await _localRepository.getAll();
    if (localCustomers.isNotNullOrEmpty) {
      return localCustomers;
    }
    final networkCustomers = await _networkRepository.getAll();
    await _localRepository.insertAll(networkCustomers);
    return networkCustomers;
  }

  @override
  Future<int> insert(Customer customer) {
    return _localRepository.insert(customer);
  }

  @override
  Future<List<int>> insertAll(List<Customer> customers) {
    // TODO: implement insertAll
    throw UnimplementedError();
  }

  @override
  Future<int> update(Customer customer) async {
    final result = await _localRepository.update(customer);
    _triggerStreamAllEvent();
    return result;
  }

  @override
  Future<List<int>> updateAll(List<Customer> customers) {
    // TODO: implement updateAll
    throw UnimplementedError();
  }

  @override
  Future<bool> delete(int id) => _localRepository.delete(id);

  @override
  Future<int> deleteAll(List<int> ids) {
    // TODO: implement deleteAll
    throw UnimplementedError();
  }
}

class CustomerLocalRepository implements Repository<Customer> {
  static final provider = Provider((ref) => CustomerLocalRepository(ref.watch(isarProvider)));

  CustomerLocalRepository(this._database);

  final Isar _database;

  @override
  Stream<List<Customer>> watchAll() {
    debugPrint('[$runtimeType] streamAll()');
    return _database.customers.where().watch();
  }

  @override
  Stream<Customer?> watch(int id) {
    debugPrint('[$runtimeType] stream($id)');
    return _database.customers.where().idEqualTo(id).watch(fireImmediately: true).map((list) => list.firstOrNull);
  }

  @override
  Future<Customer?> get(int id) {
    debugPrint('[$runtimeType] get($id)');
    return _database.customers.get(id);
  }

  @override
  Future<List<Customer>> getAll() {
    debugPrint('[$runtimeType] getAll()');
    return _database.customers.where().findAll();
  }

  @override
  Future<int> insert(Customer customer) {
    debugPrint('[$runtimeType] insert($customer)');
    return _database.writeTxn(() => _database.customers.put(customer));
  }

  @override
  Future<List<int>> insertAll(List<Customer> customers) {
    debugPrint('[$runtimeType] insertAll($customers)');
    return _database.writeTxn(() => _database.customers.putAll(customers));
  }

  @override
  Future<int> update(Customer customer) {
    debugPrint('[$runtimeType] update($customer)');
    return _database.writeTxn(() => _database.customers.put(customer));
  }

  @override
  Future<List<int>> updateAll(List<Customer> customers) {
    debugPrint('[$runtimeType] updateAll($customers)');
    return _database.writeTxn(() => _database.customers.putAll(customers));
  }

  @override
  Future<bool> delete(int id) {
    debugPrint('[$runtimeType] delete($id)');
    return _database.writeTxn(() => _database.customers.delete(id));
  }

  @override
  Future<int> deleteAll(List<int> ids) {
    debugPrint('[$runtimeType] deleteAll($ids)');
    return _database.customers.deleteAll(ids);
  }
}

class CustomerNetworkRepository implements Repository<Customer> {
  static const path = '/customers';
  static final provider = Provider((ref) => CustomerNetworkRepository(ref.read(NetworkApi.provider)));

  CustomerNetworkRepository(this._networkApi);

  final NetworkApi _networkApi;

  @override
  Stream<List<Customer>> watchAll() {
    // TODO: implement streamAll
    throw UnimplementedError();
  }

  @override
  Stream<Customer?> watch(int id) {
    // TODO: implement stream
    throw UnimplementedError();
  }

  @override
  Future<Customer?> get(int id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<Customer>> getAll() async => (await _networkApi.getMany(path)).map(Customer.fromJson).toList();

  @override
  Future<int> insert(Customer customer) async {
    final response = await _networkApi.post(customer.toJson(), path);
    return response.isNotEmpty ? 1 : 0;
  }

  @override
  Future<List<int>> insertAll(List<Customer> customers) {
    // TODO: implement insertAll
    throw UnimplementedError();
  }

  @override
  Future<int> update(Customer customer) {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<List<int>> updateAll(List<Customer> customers) {
    // TODO: implement updateAll
    throw UnimplementedError();
  }

  @override
  Future<bool> delete(int id) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<int> deleteAll(List<int> ids) {
    // TODO: implement deleteAll
    throw UnimplementedError();
  }
}
