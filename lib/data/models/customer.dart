import 'package:dartx/dartx.dart';
import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';

part 'customer.g.dart';

/// To generate @JsonSerializable classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@Collection()
@JsonSerializable()
class Customer {
  Customer({
    required this.firstName,
    required this.lastName,
    this.phone,
    this.email,
    this.address1,
    this.address2,
    this.postalCode,
    this.city,
  });

  factory Customer.empty() => Customer(firstName: '', lastName: '');

  Id id = Isar.autoIncrement;
  String firstName;
  String lastName;
  String? phone;
  String? email;
  String? address1;
  String? address2;
  String? postalCode;
  String? city;

  String get name {
    String result = '';
    if (firstName.isNotBlank) {
      result += firstName;
      if (lastName.isNotBlank) result += ' $lastName';
    } else if (lastName.isNotBlank) {
      result += lastName;
    }
    return result;
  }

  String get initials {
    String result = '';
    if (firstName.isNotBlank) result += firstName[0].toUpperCase();
    if (lastName.isNotBlank) result += lastName[0].toUpperCase();
    return result;
  }

  String get fullAddress =>
      '${address1.isNotNullOrBlank ? '$address1\n' : ''}${address2.isNotNullOrBlank ? '$address2\n' : ''}${postalCode.isNotNullOrBlank ? '$postalCode ' : ''}${city ?? ''}';

  String get subtitle {
    if (phone.isNotNullOrBlank) return phone!;
    if (email.isNotNullOrBlank) return email!;
    if (city.isNotNullOrBlank) return city!;
    return '';
  }

  @override
  String toString() => 'Customer $id: "$name"';

  factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerToJson(this);
}
