import 'package:scaffold_riverpod/data/access/customer_repository.dart';
import 'package:scaffold_riverpod/data/access/network_api.dart';
import 'package:scaffold_riverpod/data/mocks/mocked_customers.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';

class MockedNetworkApi extends NetworkApi {
  static final latency = 1.seconds;

  @override
  Future<Map<String, dynamic>> get(int id, String path) async {
    debugPrint('$runtimeType get $id\n$path/$id');
    await Future.delayed(latency);
    return Future.value(_getData(id, path));
  }

  @override
  Future<List<Map<String, dynamic>>> getMany(String path) async {
    debugPrint('$runtimeType getMany\n$path');
    await Future.delayed(latency);
    return _getManyData(path);
  }

  @override
  Future<Map<String, dynamic>> post(Map<String, dynamic> data, String path) async {
    debugPrint('$runtimeType post: $data\n$path');
    await Future.delayed(latency);
    _dataByPath[path]!.add(data);
    return Future.value({});
  }

  static final Map<String, List<dynamic>> _dataByPath = {
    CustomerNetworkRepository.path: mockedCustomersData,
  };

  static Map<String, dynamic>? _getData(int id, String path) =>
      _dataByPath.containsKey(path) ? _dataByPath[path]!.firstOrNullWhere((mock) => mock.id == id) : null;
  static List<Map<String, dynamic>> _getManyData(String path) =>
      _dataByPath.containsKey(path) ? _dataByPath[path] as List<Map<String, dynamic>> : <Map<String, dynamic>>[];
}
